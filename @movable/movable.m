classdef movable < handle
    %MOOVABLE initialize tango attribute and defines ancillary properties
    %   
    %  tango attribute control object
    % 
    %  get method applies calibration, returns rad if calibration is rad/A
    %  set function :
    %       - input in calibrated units,
    %       - check limits
    %       - waits for setpoint to be reached.
    %  
    %  object stores also:
    %  min    minimum allowed setting (tango minimum if not provided or too small)
    %  max    maximum allowed setting (tango maximum if not provided or too large)
    %  value_at_init    initial value of attribue when object created.
    %  set_tolerance    difference tolerance to check for setting point 
    %  calibration      scalar calibration value
    %
    
    properties
        attr_name       % name of attribute in Tango
        attribute       % attribute object
        min             % minimum value
        max             % maximum value
        calibration     % calibration rad/A, [1/m]/A, ...
        set_tolerance   % tolerance for reaching set point
        value_at_init   % value of moovable at initialization
    end
    
    methods
        function obj = movable(name,varargin)
            %MOVABLE Construct an instance of this class
            %   a MOVABLE is an object that has:
            %
            %   name: TAGO Attribute name
            %   limits: [2x1] limits of tunability of attribute value
            %           defaults: attribute.attrinfo.min_value, attrinfo.max_value
            %   calibration: array of scalars of calibration [units]/A
            %           defaults: 1
            %   set_tolerance: array of scalars of calibration [units]/A
            %   value_at_init: value of attribute at init of class
            %
            % ex1:
            % rf = moovable(['srrf/master-oscillator/1/Frequency'],'absolute',true,'limits',[0 352e6]);
            % ex2: 
            % rf = moovable(['srrf/master-oscillator/1/Frequency'],'absolute',false,'limits',[0.9 1.1]);
            % ex3:
            % empt = moovable('');
            %
            %see also: 
           
            p = inputParser;
            
            addRequired(p,'name',@ischar);
            addOptional(p,'absolute',true,@islogical);
            addOptional(p,'limits',[],@isnumeric);
            addOptional(p,'calibration',[],@isnumeric);
            addOptional(p,'set_tolerance',[],@isnumeric);
            
            parse(p,name,varargin{:});
            
            obj.attr_name = p.Results.name;
            
            % deal with empty moovable
            if isempty(obj.attr_name)
                warning('empty movable')
                
                obj.attribute.value = NaN;    % always return NaN
                obj.attribute.set = NaN;      % define a set field, will store the value set, but no effect on .value.
                obj.attribute.attrinfo.min_value = -Inf; % no limits
                obj.attribute.attrinfo.max_value = Inf;
                
            else
                try
                    obj.attribute = tango.Attribute(obj.attr_name);
                catch err
                    disp(err)
                    error('please provide a correct attribute name domain/family/member/attribute ');
                end
            end
            
            
            % define default calibration to 1.
            if isempty(p.Results.calibration)
                calib = ones(size(obj.attribute.value));
            else
                calib = p.Results.calibration;
                if ~ (any(size(calib) == size(obj.attribute.value)) | any(size(calib) == [1 1]))
                    error('wrong size of calibration array');
                end
            end
            obj.calibration = calib;
            
            % get present value of variable
            v0 = obj.get;
            obj.value_at_init = v0;
                    
            % default absolute limits
            limits = p.Results.limits;
            
            if isempty(limits) % absolute empty limits
                limits(1) = obj.attribute.attrinfo.min_value;
                limits(2) = obj.attribute.attrinfo.max_value;
                % relatie limits
                if ~p.Results.absolute % relative empty limits
                    limits(1) = limits(1)./ obj.value_at_init;
                    limits(2) = limits(2)./ obj.value_at_init;
                end
            else % not empty limits
                if ~p.Results.absolute % relative 
                    limits(1) = limits(1).* obj.value_at_init;
                    limits(2) = limits(2).* obj.value_at_init;
                end
            end
            
            % Tango limits are the real maximum and minimum
            if limits(1)<obj.attribute.attrinfo.min_value
                limits(1)=obj.attribute.attrinfo.min_value;
            end
            if limits(2)>obj.attribute.attrinfo.max_value
                limits(2)=obj.attribute.attrinfo.min_value;
            end
            
            obj.min = limits(1); % absolute minimum
            obj.max = limits(2); % absolute maximum
            
            % set tolerance for set to end readvalue = setpoint
            if isempty(p.Results.set_tolerance)
                obj.set_tolerance = 1; %
            else
                obj.set_tolerance = p.Results.set_tolerance; %
            end
            
        end
        
        % read attribute value
        function v = get(obj)
            % read attribute value in calibrated units
           v = obj.attribute.set .* obj.calibration; % this should be .SET?
        end
        
        
        function set(obj,value)
            % set attribute values in calibrated units, 
            % check for limits 
            % wait for set point to be reached.
            % 
            
             
            above = value>obj.max;
            if any(above)
                
                value(above) = obj.max;
                warning(['Found ' num2str(length(find(above))) ' above maxium (' num2str(obj.max) '). Set to maximum.'])
            end
            
            below = value<obj.min;
            if any(below)
                
                value(below) = obj.min;
                warning(['Found ' num2str(length(find(below))) ' below minimum (' num2str(obj.min) '). Set to minimum.'])
            end
            
            % apply calibration
            value_calib = value ./ obj.calibration;
           
            % set value to attribute
            obj.attribute.set = value_calib;
            
            % wait for set point reached
            v=obj.get;
                
            while any(abs(v-value) > obj.set_tolerance)
                if length(v)>1
                    distance = v-value;
                else
                    distance = std(v-value);
                end
                disp(['waiting for set point to be reached. distance: ' num2str(distance)])
                pause(0.1);
                v=obj.get;
                
            end
            
        end
        
        function restore_initial_values(obj)
            %restore values stored at device init
            obj.set(obj.value_at_init);
        end
        
    end
end

