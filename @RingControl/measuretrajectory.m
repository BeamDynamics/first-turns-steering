function [t,errt] = measuretrajectory(obj)
%MEASURETRAJECTORY measures trajectory for 2 turns
%
% read BPMs TbT to obtain trajectory in ring at BPM
%
% average of obj.n_acquisitions reading
% first turn obj.first_turn_index data stored for obj.nturns.
% to change number of turns use obj.setTurns(Nturns)
%
% OUTPUT: 
% 3x(NbpmxNturns) matrix.
% first row     = n horizontal turns mean of obj.n_acquisitions reading
% second row    = n vertical turns  mean of obj.n_acquisitions reading
% third row     = n sum signals, NAN if not good BPM  mean of obj.n_acquisitions reading
%
% 3x(NbpmxNturns) matrix.
% first row     = n horizontal turns std of obj.n_acquisitions reading
% second row    = n vertical turns std of obj.n_acquisitions reading
% third row     = n sum signals, NAN if not good BPM std of obj.n_acquisitions reading
%
% Requires :
% Gun On and RIPS Running. in not in good state, required to start
% Ke in single pulse, (switched on only for obj.machine='esrf-sr')
% Linac, syinj, syrf, RIPS, (srinj) : On
%
%see also: RingControl.plottrajectory RingControl.KillBeamAtGivenCurrent

% read n times orbit for 2 turns

filename = obj.TrajFileName;

% get data synchronized to KE for 2 turns, average over n acquisitions

average = obj.n_acquisitions;
selturns = obj.selturns; %[18,19]; % first and second turn in buffer

simulator = strcmp(obj.machine,'ebs-simu');

switch_on_gun_rips = false;
    
%% set up injector
if ~simulator % if real accelerator
    
    switch_on_gun_rips = true;
    
    gun  = tango.Device(obj.gun);
    rips = tango.Device(obj.rips);
    
    ripsrunning = strcmp(rips.State , 'Running');
    gunfiring = strcmp(gun.State , 'On');
    
    if  ripsrunning && gunfiring
        switch_on_gun_rips = false;
    end
    
    if  ripsrunning && ~gunfiring
        answer = questdlg('Gun is NOT On', ...
            'Switch On Gun please:', ...
            'I did it','Set Gun On','Stop measure','Set Gun On');
        
        switch answer
            case 'I did it'
                disp('Thank you for switching on the gun. ');
            case 'Set Gun On'
                gun.On();
                disp('gun On command sent');
            case 'Stop measure'
                disp('stop measurement');
                t = NaN(3,length(obj.indBPM)*obj.nturns);
                errt = t;
                return
        end
        disp('In this case this function will not handle gun On/Off and Rips Start/Stop.')
           
        switch_on_gun_rips = false;
    end
    
    if  ~ripsrunning && gunfiring
        questdlg('Rips is NOT Running', ...
            'Start Rips Rump please:', ...
             'I did it','Start RIPS Ramp','Stop measure','Start RIPS Ramp');
        
        switch answer
            case 'I did it'
                disp('Thank you for switching on the gun. ');
            case 'Start RIPS Ramp'
                rips.StartRamping();
                disp('RIPS StartRamping command sent');
            case 'Stop measure'
                disp('stop measurement');
                t = NaN(3,length(obj.indBPM)*obj.nturns);
                errt = t;
                return
        end
        disp('In this case this function will not handle gun On/Off and Rips Start/Stop.')
           
        switch_on_gun_rips = false;
    end
    
    % shoot beam: KE 1 shot, gun
    
    % set up devices for single shot, TbT injection
    Ke      = tango.Device(obj.ke);
    gun     = tango.Device(obj.gun);
    rips    = tango.Device(obj.rips);
  
end
%pn = Ke.PulseNumber.read;
%Ke.PulseNumber = 1; % set single pulse for extraction


if switch_on_gun_rips & ~simulator
    pause(1); % rips still moving.
    rips.StartRamping()
    gun.On();
    pause(1); % wait fordev_CT=tango.Device('sr/d-ct/1');
end

%% average BPM readings
for i=1:average
    
    % read BPM TbT buffer counter
    data_counter = tango.Attribute(obj.bpm_trigger_counter).value;
    
    pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
               % necessary!! or could read two identical buffers!
               
    % trigger injection and data acquisition
    if strcmp(obj.machine,'esrf-sr') & ~simulator
        Ke.On(); % dvcmd(Ke(1),'DevOn');
    end
    
    % wait for trigger couter to change
    integralwaited = 0.0;
    dt =0.2;
    while tango.Attribute(obj.bpm_trigger_counter).value == data_counter
        disp('waiting for fresh data');
        pause(dt);
        integralwaited = integralwaited + dt;
        if integralwaited>10
            warning('Waiting too long for new data, take what is available');
            break
        end
        disp(['next data ' num2str(tango.Attribute(obj.bpm_trigger_counter).value) ' before Ke: ' num2str(data_counter)]);
    end
    
    % set nan on BPMs that do not see signal
    switch obj.machine
        case 'esrf-sr'
            
            % read BPM Turn-by-Turn
            hor = tango.Attribute(obj.hor_bpm_TBT).value;
            ver = tango.Attribute(obj.ver_bpm_TBT).value;
            sig = tango.Attribute(obj.sum_bpm_TBT).value;
            sumsignal = sig(:,obj.first_turn_index); % measured signal
            
%             %shift to stick to the simulated data % lattice changed, should not be needed any more
%             hor = circshift(hor,-1);
%             ver = circshift(ver,-1);
             
            bpm_with_beam = (sumsignal-obj.sum_signal_background)./...
                (obj.sum_signal_beam-obj.sum_signal_background);
            
            indlastBPM = find(bpm_with_beam>0.6,1,'last');
            
        case 'esrf-sy'
            
            [hor,ver,sig] = obj.ReadTDPSpark;
            
            bpm_with_beam = ones(size(sumsignal));
            
            bpm_with_beam(sig < 0.5e6) = 0 ;
            
            indlastBPM = find(bpm_with_beam>0.15,1,'last');
            
        case 'ebs-simu'
            try
                if (obj.noise)
                    attr = tango.Attribute(obj.initial_coordinates);
                    attr.set = [obj.injoff, 0,0,0,0,0]+randn(1,6).*obj.injectorRMSNoise;
                end
                pause(1);
                % read BPM Turn-by-Turn
                hor = tango.Attribute(obj.hor_bpm_TBT).value;
                ver = tango.Attribute(obj.ver_bpm_TBT).value;
                sig = ones(size(hor));  %no sum signal in simulator at the moment
                
                % warning('logic/scalar error && -> & !')
                bpm_with_beam = ~isnan(hor) & ~isnan(ver) ;
            catch err
                disp(err)
                error('ERROR RingControl.measuretrajectory: did you set BPM TbT?')
                return
            end
            indlastBPM = find(bpm_with_beam==0,1,'first');
            if isempty(indlastBPM)
                indlastBPM = size(hor,1);
            end
            
    end
    
    NBPM = size(hor,1);
    nturns = size(hor,2);
    
    disp(['aqn ' num2str(i) ' of ' int2str(nturns) ...
        ' turns, signal on ' int2str(length(find(bpm_with_beam>0))) ...
        ' / ' int2str(NBPM*nturns) ' BPMs']);
    
    %[~,indlastBPM]=max(abs(diff(bpm_with_beam)));
    
    thi(:,i)=reshape(hor(:,selturns),1,[]);
    tvi(:,i)=reshape(ver(:,selturns),1,[]);
    tsi(:,i)=reshape(sig(:,selturns),1,[]);
    
    if indlastBPM < length(obj.indBPM)
        thi(indlastBPM:end,i)=NaN;
        tvi(indlastBPM:end,i)=NaN;
        tsi(indlastBPM:end,i)=NaN;
    end
    
    % kill beam if stored current
    obj.KillBeamAtGivenCurrent;
    
end

if switch_on_gun_rips & ~simulator
    pause(0.25); % rips still moving.
    rips.StopRamping()
    gun.Off(); % limit dose
    %Ke.PulseNumber = pn; % set back initial number of pulses for extraction
end

th = mean2(thi,2)';
tv = mean2(tvi,2)';
ts = mean2(tsi,2)';

t = [th;tv;ts];

eth = std2(thi,1,2)';
etv = std2(tvi,1,2)';
ets = std2(tsi,1,2)';

errt = [eth;etv;ets];

% convert to m
if ~simulator
   % warning('reading in mm or m?')

    t = t*1e-3; % [m]
    errt = errt*1e-3; % [m]
    
end

%% save and store data

% read correctors settings
hs = obj.sh.get;
vs = obj.sv.get;
sp = [obj.s12.get, obj.s3.get];
cv = [obj.cv8.get, obj.cv9.get];
freq = obj.rf.get;

% save data
if obj.savefile
    save(fullfile(obj.MeasurementFolder,...
        ['FirstTurnsTrajectory' datestr(now,'YYYYmmmDD_HHMMSS') filename]),...
        't','errt','hs','vs','sp','cv','freq');
end

obj.last_measured_trajectory = t;

end
