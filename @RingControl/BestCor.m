function [dc, ibest]= BestCor(RM,X)
% get best corrector and its strength 
%
% INPUTS:
% -RM : response matrix (NxM)
% - X : vector to be corrected (Nx1)
% 
% OUTPUTS:
% - dc : delta correctors strengths, all zeros apart the one of the best cor.
% - idbest : index of the best corrector
%
%

scalefact = RM\X; % vector of corrections factors [m]/[m/rad] = [rad]

corstd = zeros(size(scalefact));
for ic = 1:length(scalefact)
    Y = scalefact(ic)*RM(:,ic);
    corstd(ic) = std(X-Y);
end

[~,ibest] = min(corstd); % 

dc = zeros(size(scalefact));

dc(ibest) = scalefact(ibest);

doplot = false;
if doplot
    figure;
    plot(X);
    hold on; 
    plot(scalefact(ibest)*RM(:,ibest)); 
    plot(X-scalefact(ibest)*RM(:,ibest));
    legend('measured','correction','final');
    xlabel('BPM #');
end

end
