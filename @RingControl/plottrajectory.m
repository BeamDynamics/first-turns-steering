function plottrajectory(obj,varargin)
%PLOTTRAJECTORY display last measurement of trajectory
%
%see also: RingControl

p=inputParser;
addRequired(p,'obj');
addParameter(p,'figure',true,@islogical);
parse(p,obj,varargin{:});

if p.Results.figure
    figure;
end

% get tunes
[tunesfftx] = findtunetom2(obj.last_measured_trajectory(1,~isnan(obj.last_measured_trajectory(1,:)))',200,1);
[tunesffty] = findtunetom2(obj.last_measured_trajectory(2,~isnan(obj.last_measured_trajectory(1,:)))',200,1);
tunesfft =[tunesfftx tunesffty];

lastbpm=find(~isnan(obj.last_measured_trajectory(1,:)),1,'last');
[l,t,~]=atlinopt(obj.rmodel,0,1:obj.indBPM(min(length(obj.indBPM),lastbpm)));
phlastbpm=l(end).mu./2./pi;

plot(obj.last_measured_trajectory([1,2],:)','LineWidth',3);
xlabel('BPM #')
ylabel('[m]');
ax =gca;
ax.FontSize =14;
ax.Color='None';
ax.XLim=[0,lastbpm];
ax.Title.String={['tunes expected: ' num2str(t)];...
    ['tunes fft: ' num2str(tunesfft)];...
    ['expected \mu@BPM' num2str(min(length(obj.indBPM),lastbpm)) ': ' num2str(phlastbpm)];...
    ['']};
yyaxis right
plot(obj.last_measured_trajectory(3,:)','LineWidth',3);
ylabel('sum signal');
grid on;
ax =gca;
ax.FontSize =14;
ax.Color='None';
ax.XLim=[0,lastbpm];
f = gcf;
f.Color=[1 1 1];