function [h,v,s,pt,st] = ReadTDPSpark(obj)
% read TDP data
% in spark manager: 
% Clock Delay = 1e-6
% TDP = True
% TDP_QSum Enable
%
% OUTPUT:
%         h : horizontal positions at requested turns
%         v: vertical position at requested turns
%         s: sum signal at requested turns (needs TDP_QSum_Enable)
%         pt : matrix readings vs turns
%         st : matrix sum signal and inchoerency vs turns
% 
% SPARK manager must have Closck delay at 1e-6 (or better zero if possible)
% SPARK manager -> Expert -> edit settings -> TDP
% SPARK manager -> Expert -> edit settings -> TDP_QSum_Enable
% SPARK manager -> Expert -> edit settings -> Acquisition duration 0.02
% SPARK manager -> Expert -> edit settings -> Closck Delay 0.00001
% SPARK manager -> Expert -> edit settings -> Decimation Enable FALSE
% 
%see also: 

turnnum = obj.selturns(end)+1;

pos = tango.Attribute('sy/d-bpmlibera-sp/all/All_TDP_Positions').value;
sumsig = tango.Attribute('sy/d-bpmlibera-sp/all/All_TDP_QSUM').value;

p = pos';
pt=reshape(p(:),[],150);

pt =pt*1e-6; % convert to m

h = pt(turnnum,1:75)';
v = pt(turnnum,75+(1:75))';

s = sumsig';
st=reshape(s(:),[],150);

s = st(turnnum,1:75)';

end
