function InitEsrfSR(obj)
%SWITCHTOEBSSIMULATOR turns parameters of First2Turns class to EBS
%simulator
%
% 
%
%see also: First2Turns

TANGO_HOST='tango://orion:10000/';

obj.MeasurementFolder = pwd;

obj.first_turn_index = 18;

%% movables
% attributes to use. Defaults for: ESRF SR Aug 2018
obj.s12 = movable([TANGO_HOST 'sr/ps-si/12/Current'],'calibration',43.8e-3 / 10e3,'absolute',true,'limits',[8500 9000].*(43.8e-3 / 10e3));
obj.s3 = movable([TANGO_HOST 'sr/ps-si/3/Current'],'calibration',21.6431e-3 / 8e3,'absolute',true,'limits',[6200 6900].*(21.6431e-3 / 8e3));
%obj.cur_s12_set = 'sr/ps-si/12/Current';
%obj.cur_s3_set = 'sr/ps-si/3/Current';
obj.cv8 = movable([TANGO_HOST 'tl2/ps-c1/cv8/Current']);
obj.cv9 = movable([TANGO_HOST 'tl2/ps-c1/cv9/Current']);
obj.sh =  movable([TANGO_HOST 'sr/st-h/all/Current'],'calibration',load_corcalib('sr',8),'limits',[-3e-4 3e-4]);
obj.sv =  movable([TANGO_HOST 'sr/st-v/all/Current'],'calibration',load_corcalib('sr',9),'limits',[-3e-4 3e-4]);
obj.rf =  movable([TANGO_HOST 'sy/ms/1/frequency'],'absolute',true,'limits',352202077 + [-300 +300]);
obj.quad = movable('');
obj.sext = movable('');
obj.octu = movable('');

%% devices to use
obj.ke = [TANGO_HOST 'sy/ps-ke/1'];
obj.gun = [TANGO_HOST 'elin/beam/run'];
obj.rips = [TANGO_HOST 'sy/ps-rips/manager'];

%% diagnostics
obj.scraper = [TANGO_HOST 'sr/d-scr/c4-ext'];

obj.stored_current = [TANGO_HOST 'sr/d-ct/1'];

obj.hor_bpm_TBT = [TANGO_HOST 'sr/d-bpmlibera/all/X_Position_DD'];
obj.ver_bpm_TBT = [TANGO_HOST 'sr/d-bpmlibera/all/Z_Position_DD'];
obj.sum_bpm_TBT = [TANGO_HOST 'sr/d-bpmlibera/all/Sum_DD'];

obj.hor_bpm_SA = [TANGO_HOST 'sr/d-bpmlibera/all/X_Position_SA'];
obj.ver_bpm_SA = [TANGO_HOST 'sr/d-bpmlibera/all/Z_Position_SA'];
obj.sum_bpm_SA = [TANGO_HOST 'sr/d-bpmlibera/all/All_SA_Sum'];

obj.bpm_trigger_counter = [TANGO_HOST 'sr/d-bpmlibera/c1-6/DDTriggerCounter'];

obj.status_bpm =[TANGO_HOST 'sr/d-bpmlibera/all/All_Status']; % BPM status (0 means no error, so ok)
obj.state_hst =[TANGO_HOST 'sr/st-h/all/SteererStates']; % H steerer status (ON OFF FAULT,...)
obj.state_vst =[TANGO_HOST 'sr/st-v/all/SteererStates']; % V steerer status (ON OFF FAULT,...)
obj.orbit_cor_disabled_bpm =[TANGO_HOST 'sr/svd-orbitcor/h/DisabledBPMsIndex']; % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =[TANGO_HOST 'sr/svd-orbitcor/h/DisabledActuatorsIndex']; % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =[TANGO_HOST 'sr/svd-orbitcor/v/DisabledActuatorsIndex']; % V steerer not in orbit correction
obj.autocor = [TANGO_HOST 'sr/svd-orbitcor/auto'];

%load reference sum signal
a=load('/users/beamdyn/dev/commissioningtools/@RingControl/referenceSRBPMsumsignal.mat','sumsignal');
obj.sum_signal_beam = a.sumsignal; % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(size(a.sumsignal));
        
obj.h_tune = [TANGO_HOST 'sr/d-tm/1/Qh'];
obj.v_tune = [TANGO_HOST 'sr/d-tm/1/Qv'];
obj.h_emittance = [TANGO_HOST 'sr/d-emit/average/Xemittance'];
obj.v_emittance =[TANGO_HOST 'sr/d-emit/average/Zemittance'];
obj.lifetime =[TANGO_HOST 'sr/d-ct/1/Lifetime'];

obj.neutrons = [TANGO_HOST 'sr/neutron/all/Dose'];

%% lattice
% ESRF-SR-Aug-2018
obj.kick = [...
    0.000229329191157   0.000189098882818   0.000189098882581   0.000229329190670;... -1 mm
    0.001178019429289   0.000883503129909   0.000883503129734   0.001178019429700;... -5 mm
    0.002432690474176   0.001618761106534   0.001618761107562   0.002432690473300;... -10 mm
    0.003760982590457   0.002217214220177   0.002217214220032   0.003760982588687;... -15 mm
    0.004035202787380   0.002321516928723   0.002321516927466   0.004035202785198;... -16 mm
    0.004312218089636   0.002420912012346   0.002420912010716   0.004312218087133;... -17 mm
    0.004592003013566   0.002515495468220   0.002515495466340   0.004592003010427]; % -18 mm

% - 16mm kickers bump
obj.K3 = obj.kick(2,3);
obj.K4 = obj.kick(2,4);
obj.injoff = -6.0*1e-3;

%latticefile = '/users/beamdyn/dev/commissioningtools/@RingControl/ESRFInj.mat'; % ring lattice
%a = load(latticefile);

mod = sr.model; % load lattice from operation/appdata/optics/settings/theory
obj.rmodel = mod.ring;
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = mod.get(0,'steerh')';
obj.indVst = mod.get(0,'steerv')';
obj.indQuad = mod.get(0,'qp')';
obj.indSext = mod.get(0,'sx')';
obj.indOctu = [];

obj.rmodel = atsetfieldvalues(obj.rmodel,obj.indHst,'KickAngle',{1,1},0);
obj.rmodel = atsetfieldvalues(obj.rmodel,obj.indHst,'KickAngle',{1,2},0);
obj.rmodel = atsetfieldvalues(obj.rmodel,obj.indVst,'KickAngle',{1,1},0);
obj.rmodel = atsetfieldvalues(obj.rmodel,obj.indVst,'KickAngle',{1,2},0);
indK1 = find(atgetcells(obj.rmodel,'FamName','SDBD'),1,'last');
indK2 = find(atgetcells(obj.rmodel,'FamName','SD3U'),1,'last');
indK3 = find(atgetcells(obj.rmodel,'FamName','SD3U'),1,'first');
indK4 = find(atgetcells(obj.rmodel,'FamName','SDBD'),1,'first');
obj.rmodel = atsetfieldvalues(obj.rmodel,indK1,'FamName','DR_K1');
obj.rmodel = atsetfieldvalues(obj.rmodel,indK2,'FamName','DR_K2');
obj.rmodel = atsetfieldvalues(obj.rmodel,indK3,'FamName','DR_K3');
obj.rmodel = atsetfieldvalues(obj.rmodel,indK4,'FamName','DR_K4');
obj.rmodel = atsetfieldvalues(obj.rmodel,[indK1,indK2,indK3,indK4],'PassMethod','StrMPoleSymplectic4Pass');
obj.rmodel = atsetfieldvalues(obj.rmodel,[indK1,indK2,indK3,indK4],'MaxOrder',1);
obj.rmodel = atsetfieldvalues(obj.rmodel,[indK1,indK2,indK3,indK4],'NumIntSteps',10);
obj.rmodel = atsetfieldvalues(obj.rmodel,[indK1,indK2,indK3,indK4],'PolynomA',{1,1},0);
obj.rmodel = atsetfieldvalues(obj.rmodel,[indK1,indK2,indK3,indK4],'PolynomB',{1,1},0);
obj.rmodel=atsetcavity(atradon(obj.rmodel),8e6,1,992);
% obj.rmodel = a.ring;
% obj.indBPM=findcells(obj.rmodel,'Class','Monitor');
% obj.indHst=findcells(obj.rmodel,'KickAngle');
% obj.indVst=obj.indHst;
% obj.indQuad=findcells(obj.rmodel,'Class','Quadrupole');

tl2file = '/users/beamdyn/dev/commissioningtools/@RingControl/TL2.mat';
b = load(tl2file,'TL2');
obj.tl2model = b.TL2;

end
