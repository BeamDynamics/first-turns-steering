function [o,erro] = measureorbit(obj,varargin)
%MEASUREORBIT measures closed orbit
%
% read BPMs to obtain orbit in ring at BPM
%
% average of obj.n_acquisitions reading
% 
% Optional inputs:
% 'ignoreinch'  = ignores inchoerency. (NOT YET IMPLEMENTED, ALWAYS IGNORED)
% 'algorithm'   = BPM algorithm
% 
% OUTPUT:
% 3x(Nbpm) matrix.
% first row     =  horizontal mean of obj.n_acquisitions reading
% second row    =  vertical   mean of obj.n_acquisitions reading
% third row     =  sum signals, NAN if not good BPM  mean of obj.n_acquisitions reading
%
% 3x(Nbpm) matrix.
% first row     = horizontal  std of obj.n_acquisitions reading
% second row    = vertical    std of obj.n_acquisitions reading
% third row     = sum signals, NAN if not good BPM std of obj.n_acquisitions reading
%
% Requires :
%
%see also: RingControl.plotorbit

p = inputParser;

addParameter(p,'ignoreinch',false);
addParameter(p,'algorithm',0);

parse(p,varargin{:});

ignoreinchoerency = p.Results.ignoreinch;
algorithm = p.Results.algorithm;

if ~ignoreinchoerency
    warning('ignore inchoerency not yet functional!');
end

if algorithm == 0
      warning('algorithm not yet functional!');
end

filename = obj.OrbitFileName;

average = obj.n_acquisitions;

simulator = strcmp(obj.machine,'ebs-simu');

%% average BPM readings
for i=1:average
        % read BPM TbT buffer counter
    data_counter = tango.Attribute(obj.bpm_trigger_counter).value;
    
    pause(1.0) % necessary!! or later trigger counter re-reading will be already after KE shot!
               % necessary!! or could read two identical buffers!
               
    % wait for trigger couter to change
    integralwaited = 0.0;
    dt =0.2;
    while tango.Attribute(obj.bpm_trigger_counter).value == data_counter
        disp('waiting for fresh data');
        pause(dt);
        integralwaited = integralwaited + dt;
        if integralwaited>10
            warning('Waiting too long for new data, take what is available');
            break
        end
        disp(['next data ' num2str(tango.Attribute(obj.bpm_trigger_counter).value) ' last measure: ' num2str(data_counter)]);
    end
    pause(2.0);
    try
        % set nan on BPMs that do not see signal
        switch obj.machine
            case {'esrf-sr', 'esrf-sy'}
                
                % read BPM
                hor = tango.Attribute(obj.hor_bpm_SA).value;
                ver = tango.Attribute(obj.ver_bpm_SA).value;
                sig = tango.Attribute(obj.sum_bpm_SA).value;
                
            case 'ebs-simu'
                % read BPM
                hor = tango.Attribute(obj.hor_bpm_SA).value;
                ver = tango.Attribute(obj.ver_bpm_SA).value;
                sig = ones(size(hor)); % signal not available in simulator tango.Attribute(obj.sum_bpm_SA).value;
                
        end
    catch err
        disp(err)
        error('Did you set BPM in Slow Acquisiton mode? (SA*)')
    end
    NBPM = size(hor,2);
    
    disp(['aqn ' num2str(i) ' of ' num2str(NBPM) ' BPMs' ]);
    
    thi(:,i)=hor;
    tvi(:,i)=ver;
    tsi(:,i)=sig;
    
end


th = mean2(thi,2)';
tv = mean2(tvi,2)';
ts = mean2(tsi,2)';

o = [th;tv;ts];

eth = std2(thi,1,2)';
etv = std2(tvi,1,2)';
ets = std2(tsi,1,2)';

erro = [eth;etv;ets];

% convert to m if not simulator
if ~simulator
   
    %o = o*1e-3; % [m]
    %erro = errt*1e-3; % [m]
    
end

%% save and store data

% read correctors settings
hs = obj.sh.get;
vs = obj.sv.get;
sp = [obj.s12.get, obj.s3.get];
cv = [obj.cv8.get, obj.cv9.get];
freq = obj.rf.get;

% save data
if obj.savefile
    save(fullfile(obj.MeasurementFolder,...
        ['COD' datestr(now,'YYYYmmmDD_HHMMSS') filename]),...
        'o','erro','hs','vs','sp','cv','freq');
end

obj.last_measured_orbit = o;

end
