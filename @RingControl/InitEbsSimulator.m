function InitEbsSimulator(obj)
%SWITCHTOEBSSIMULATOR turns parameters of First2Turns class to EBS
%simulator
%
% 
%
%see also: First2Turns


TANGO_HOST=['tango://' getenv('TANGO_HOST') '/'];

%ft = First2Turns;

obj.MeasurementFolder = pwd;
obj.first_turn_index = 1;

%% movable objects
obj.ke      = movable(''); % movable('sy/ps-ke/1'); 
obj.gun     = movable(''); % 'elin/beam/run';%% missing
obj.rips    = movable('');
obj.scraper = movable('');
obj.s12     = movable('');
obj.s3      = movable('');
obj.cv8     = movable('');
obj.cv9     = movable('');
obj.sh      = movable([TANGO_HOST 'srmag/hst/all/Strengths'],'absolute',true,'limits',[-4e-4 4e-4]);
obj.sv      = movable([TANGO_HOST 'srmag/vst/all/Strengths'],'absolute',true,'limits',[-4e-4 4e-4]);
obj.rf      = movable([TANGO_HOST 'srrf/master-oscillator/1/Frequency'],'absolute',false,'limits',[0.99 1.01]);
obj.quad    = movable([TANGO_HOST 'srmag/m-q/all/CorrectionStrengths']);
obj.sext    = movable([TANGO_HOST 'srmag/m-s/all/CorrectionStrengths']);
obj.octu    = movable([TANGO_HOST 'srmag/m-o/all/CorrectionStrengths']);

%% diagnostics
obj.stored_current = [TANGO_HOST 'srdiag/beam-current/total/Current'];

obj.hor_bpm_TBT = [TANGO_HOST 'srdiag/beam-position/all/TBT_HPositions'];
obj.ver_bpm_TBT = [TANGO_HOST 'srdiag/beam-position/all/TBT_VPositions'];
obj.initial_coordinates = [TANGO_HOST 'sys/ringsimulator/ebs/TbT_InCoord'];
obj.sum_bpm_TBT = [];

obj.hor_bpm_SA = [TANGO_HOST 'srdiag/beam-position/all/SA_HPositions'];
obj.ver_bpm_SA = [TANGO_HOST 'srdiag/beam-position/all/SA_VPositions'];
obj.sum_bpm_SA = [];

obj.bpm_trigger_counter = 'sys/ringsimulator/ebs/Counter';

obj.status_bpm =[TANGO_HOST 'srdiag/beam-position/all/All_Status']; % BPM status (0 means no error, so ok)
obj.state_hst =[TANGO_HOST 'srmag/hst/all/CorrectorStates']; % H steerer status (ON OFF FAULT,...)
obj.state_vst =[TANGO_HOST 'srmag/vst/all/CorrectorStates']; % V steerer status (ON OFF FAULT,...)
obj.orbit_cor_disabled_bpm =[TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledBPMsIndex']; % H steerer not in orbit correction
obj.orbit_cor_disabled_hst =[TANGO_HOST 'sr/beam-orbitcor/svd-h/DisabledActuatorsIndex']; % H steerer not in orbit correction
obj.orbit_cor_disabled_vst =[TANGO_HOST 'sr/beam-orbitcor/svd-v/DisabledActuatorsIndex']; % V steerer not in orbit correction
obj.autocor =[TANGO_HOST 'sr/beam-orbitcor/svd-auto'];

%load reference sum signal
obj.sum_signal_beam = ones(1,320); % sum signal with beam (sensitivity of each BPM)
obj.sum_signal_background = zeros(1,320);

obj.h_tune = [TANGO_HOST 'srdiag/beam-tune/1/Tune_h'];
obj.v_tune = [TANGO_HOST 'srdiag/beam-tune/1/Tune_v'];
obj.h_emittance = [TANGO_HOST 'srdiag/emittance/id07/Emittance_h'];
obj.v_emittance = [TANGO_HOST 'srdiag/emittance/id07/Emittance_v'];
obj.lifetime = [TANGO_HOST 'srdiag/beam-current/total/Lifetime'];
obj.neutrons = '';

%% lattice related informations
kick = [...
    0.000489845903684   0.000426168812966   0.000426168820758   0.000489845969593;... -1 mm
    0.002451840946955   0.002131227999229   0.002131228196820   0.002451842601757;... -5 mm
    0.004910225944962   0.004263696516143   0.004263697305621   0.004910232596004;... -10 mm
    0.007375181085586   0.006397873450655   0.006397875223752   0.007375196110139]; % -15 mm

% - 10mm kickers bump
K3 = 0.0;% kick(3,3);
K4 = 0.0;% kick(3,4);

obj.kick = kick;
obj.K3 = K3;
obj.K4 = K4;
obj.injoff = -0.0060; % set simulator TbT input to [injoff, 0,0,0,0,0]

mod = ebs.model('reduce',true,'keep','QF8D');
ring = mod.ring;
obj.indBPM = mod.get(0,'bpm')';
obj.indHst = sort([mod.get(0,'steerh'); mod.get(0,'dq')])';
obj.indVst = mod.get(0,'steerv')';

% mae sure steerers have KickAngle.
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indHst,'KickAngle',{1,2},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,1},0);
ring = atsetfieldvalues(ring,obj.indVst,'KickAngle',{1,2},0);

% quadrupole indexes (BAD FOR QF8D of 2PW cells)
qpind = mod.get(0,'qp');
qf8dind = mod.get(0,'qf8d');
obj.indQuad=sort([qpind; qf8dind(1:50:end)])';

obj.indSext = mod.get(0,'sx')';
obj.indOctu = mod.get(0,'oc')';

ring=atsetcavity(atradon(ring),6.5e6,1,992);
obj.rmodel = ring;


obj.tl2model ={};

try
    %% set up simulator
    attr = tango.Attribute(obj.initial_coordinates);    attr.set = [obj.injoff, 0,0,0,0,0];
catch err
    disp(err)
    error('Impossible to set off axis injection in simulator. Is Tango Available? (raki, CTRM)')
end

end