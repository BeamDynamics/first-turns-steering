classdef RingControl < handle
    %RINGCONTROL for common Beam Dynamics experiments
    %
    %   Base class to store methods and properties often used in
    %   BeamDynamics Applications.
    %
    %   Allows to:
    %   read and set correctors, septa123, CV89 (TL2),
    %   read orbit
    %   read trajectory
    %   read and set RF frequency
    %
    %   can be used for: ebs-simu, esrf-sr, esrf-sy
    %
    %   includes:
    %   commonly used methods such as svd
    %   calibration for magnets (esrf-sr, esrf-sy only)
    %
    %
    %see also: First2Turns
    
    properties
        
        machine
        
        MeasurementFolder % folder where to store data
        first_turn_index % index of first turn in TbT
        
        s12 %  S12
        s3  %  S3
        cv8 %  CV8
        cv9 %  CV9
        sh % horizontal SR steerers
        sv % vertical SR steerers
        rf % RF frequency
        quad % quadrupoles
        sext % sextupoles
        octu % octupoles
        
        ke % device to control KE
        gun % device to control Gun
        rips % device to control RIPS
        
        scraper % scraper device
      
        % TbT Data
        hor_bpm_TBT % horizontal BPM attribute
        ver_bpm_TBT % vertical BPM attribute
        sum_bpm_TBT % BPM sum attribute
        
        % Slow Acquisition Data
        hor_bpm_SA % horizontal BPM attribute
        ver_bpm_SA % vertical BPM attribute
        sum_bpm_SA % BPM sum attribute
        
        bpm_trigger_counter % counter that changes with fresh BPM data
        
        sum_signal_beam % sum signal with beam (sensitivity of each BPM)
        sum_signal_background % sum signal without beam (background)
        
        status_bpm % BPM status (0 means no error, so ok)
        state_hst % H steerer state (ON OFF FAULT,...)
        state_vst % V steerer state (ON OFF FAULT,...)
        orbit_cor_disabled_bpm  % BPM not in orbit correction
        orbit_cor_disabled_hst  % H steerer not in orbit correction
        orbit_cor_disabled_vst  % V steerer not in orbit correction
        autocor                 % autocor device
        
        h_tune          % horizontal tune
        v_tune          % vertical tune
        h_emittance     % hor. emittance
        v_emittance     % ver. emittance
        lifetime        % total lifetime
        
        neutrons        % neutron detectors (SAFETY)
        
        stored_current % current monitor device
        max_stored_current % maximum accepted current before scraper acts (used by KillBeamAtGivenCurrent)
        initial_coordinates % device to control initial coordinates given to the simulator
        
        kick % injection bump kickers strengths
        K3 % K3 strenght to set for simulated trajectory
        K4 % K4 strenght to set for simulated trajectory
        injoff % injected - stored beam offset
        
        noise % boolean to add noise to the measurements in the simulator
        injectorRMSNoise % (1x6) array of rms noise on the injecting coordinates
        
        rmodel % AT lattice model to use for simulation (RM)
        tl2model % AT lattice model of Transferline to ring
        
        savefile % flag to determine file saving on acquisition
        selturns % turns to use for correction
        n_acquisitions  % number of acquisitions
        last_measured_trajectory % store last measured trajectory
        TrajFileName % label to append to measurements
        
        last_measured_orbit % store last measured closed orbit
        OrbitFileName % label to append to measurements
        
        indHst  % indexes of horizontal steerers in AT rmodel
        indVst  % indexes of vertical steerers in AT rmodel
        indBPM  % indexes of BPM in AT rmodel
        indQuad % indexes of Quadrupoles in AT rmodel
        indSext % indexes of Sextupoles in AT rmodel
        indOctu % indexes of Octupoles in AT rmodel
        
        % properties below should be private
        
        nturns % number of turns to measure (use setTurns)
       
        
    end
    
    methods
        
        function obj = RingControl(machine)
            %RINGCONTROL Construct an instance of this class
            %
            %   Base class to store methods and properties often used in
            %   BeamDynamics Applications.
            %
            %   Allows to:
            %   read and set correctors, septa123, CV89 (TL2),
            %   read orbit
            %   read trajectory
            %   read and set RF frequency
            %
            %   can be used for: ebs-simu, esrf-sr, esrf-sy
            %
            %   includes:
            %   commonly used methods such as svd
            %   calibration for magnets (esrf-sr, esrf-sy only)
            %
            %
            %see also: First2Turns
            
            obj.machine = machine;
            obj.n_acquisitions = 1;
            obj.nturns=2;
            obj.last_measured_trajectory = [];
            
            obj.max_stored_current = 1;
            
            obj.TrajFileName='';
            
            % chose lattice to initialize
            switch machine
                case 'ebs-simu'
                    obj.InitEbsSimulator;
                    
                case 'ebs-sr'
                    obj.InitEbsSimulator;
                    
                case 'esrf-sr'
                    obj.InitEsrfSR;
                    
                case 'esrf-sy'
                    obj.InitEsrfSY;
                    
                otherwise
                    error(['Suported machines are: '...
                        '''esrf-sr'', ''esrf-sy'', ''ebs-simu'''])
            
            end
            
            % set selturns correctly
            obj.setTurns;
            
        end
        % methods defined in seprated functions
        
        %Switch Initialization to EBS simulator
        InitEbsSimulator(obj);
        
        InitEsrfSR(obj);
        
        InitEsrfSY(obj);
        
       
        %% functions lattice and Control System dependent! ESRF SR 2018
        function setTurns(obj,n)
            % define number of turns to keep for trajectory measurement
            % after the first_turn_index turn.
            % with no inputs, property nturns is used as number of turns.
            %
            if nargin<2
                n = obj.nturns;
            end
            obj.nturns = n;
            
            obj.selturns = obj.first_turn_index + (0:(obj.nturns-1)); % first 2 turns parentheses needed.
        end
        
        
        
        function AddSteererPolarityErrors(obj,ErrCalibHSt,ErrCalibVSt)
            % AddSteererPolarityErrors(obj,ErrCalibHSt,ErrCalibVSt)
            %
            %   multiply the present calibration of the steerers by the
            %   value passed as input.
            %
            %   ErrCalibHst is a vector with the same size of getHSteerersValues
            %   ErrCalibVst is a vector with the same size of getVSteerersValues
            %
            
            obj.sh.calibration = obj.sh.calibration.*ErrCalibHSt;
            obj.sv.calibration = obj.sv.calibration.*ErrCalibVSt;
            
        end
        
        
        function enabledhst = getEnabledHsteerers(obj)
            % returns a vector of true/false for correctors On and enabled
            % in orbit correction
            sta = tango.Attribute(obj.state_hst).value;
            enabledhst = strcmp(sta,'Disabled')==0;
            
            try
                codisab = tango.Attribute(obj.orbit_cor_disabled_hst).value;
            catch
                warning(['could not read : ' obj.orbit_cor_disabled_hst]);
                codisab=[];
            end
            
            enabledhst(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function enabledvst = getEnabledVsteerers(obj)
            % returns a vector of true/false for correctors On and enabled
            % in orbit correction
            sta = tango.Attribute(obj.state_vst).value;
            enabledvst = strcmp(sta,'Disabled')==0; % device not On
            
            try
                codisab = tango.Attribute(obj.orbit_cor_disabled_vst).value;
            catch
                warning(['could not read : ' obj.orbit_cor_disabled_vst]);
                codisab=[];
            end
            
            enabledvst(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function enabledbpm= getEnabledBPM(obj)
            % returns a vector of true/false for BPM with Status=0 (no error)
            %  and enabledin orbit correction
            sta = tango.Attribute(obj.status_bpm).value;
            enabledbpm = sta == 0;
            
            try
                codisab = tango.Attribute(obj.orbit_cor_disabled_bpm).value;
            catch
                warning(['could not read : ' obj.orbit_cor_disabled_bpm]);
                codisab=[];
            end
            
            enabledbpm(codisab+1) = false; % Device not in orbit correction (+1 to convert from C indexeing)
            
        end
        
        function setNoise(obj,injectorRMSNoise)
            %  setNoise(obj,injectorRMSNoise)
            %  set noise on the injected coordinates
            %
            obj.injectorRMSNoise=injectorRMSNoise;
            obj.noise=true;
        end
        
        % trajectory measurement method
        [t,errt] = measuretrajectory(obj);
        
        % display last trajectory measurement
        plottrajectory(obj,varargin);
        
        % orbit measurement method
        [t,errt] = measureorbit(obj,varargin);
        
        % display last orbit measurement
        plotorbit(obj,varargin);
        
        % booster TDP data reading and reshaping
        [h,v,s,pt,st] = ReadTDPSpark(obj);
        
        %trajectory simulation method
        [t] = SimulateTrajectoryKick(obj,indSt,kick,plane);
        
        % kill beam if above current limit
        KillBeamAtGivenCurrent(obj,varargin);
        
        % generic SVD to chose among different options
        dq = svd(obj,svd_mode,varargin);
        
    end
    
    methods (Static)
        
        % from other software in CTRM
        dq = qemsvd_Tikhonov(a,b,lambda,plot);
        dq = qemsvd(a,b,neig);
        [dq, ibest]= BestCor(RM,X);
        
        % from matlab central: Per Christian Hansen, IMM, 06/22/93.
        [U,s,V] = csvd(A,tst)
        [x_lambda,rho,eta] = tikhonov(U,s,V,b,lambda,x_0);
        
    end
    
end

