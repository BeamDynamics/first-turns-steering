function ModelRM = SimulateTrajectoryRM(obj, varargin)
% SIMULATE2TURNSRESPONSE computes a trajectory response matrix for lattice
%
% INPUT (variable):
% 'indBPM' : AT bpm indexes  (default: from obj.rmodel, Monitor)
% 'indHCor' : AT hor. correctors indexes (default: from obj.rmodel, KickAngle)
% 'indVCor' : AT ver. correctors indexes (default: from obj.rmodel, KickAngle)
% 'kick'    : [rad] kick for RM computation
% 'delta'   : [] energy variation for frequency change RM computation
% 'RF'      : use RF (logic, default: true)  
% 'rad'     : use radiation (logic, default: true)  
%
% 
% OUTPUT:
% ModelRM structure with fields:
%   struct with fields (640 BPM, 288 correctors in this case):
%
%     TrajHCor: {[640×288 double]  []  [640×288 double]}
%     TrajVCor: {[640×288 double]  []  [640×288 double]}
%     TrajHDPP: [1×640 double]
%     TrajVDPP: [1×640 double]
%         RMSP: [2×640 double]
%         RMCV: [2×640 double]
%         kval: scalar, rad, kick for RM
%        delta: scalar,[], energy deviation for RM
%
%
% ex: 
% >> ft = FirstTurns('ebs-simu');
% >> ft.setTurns(4); % set turns to compute trajetory RM
% >> ft.injoff = -6e-3; % set the injected - stored beam offset.
% >> TrajRM = ft.SimulateTrajectoryRM('RF',false,'rad',true); % simulate trajectory RM with RF off.
% >> ft.ModelRM % display computed trajectory RMs
% 
%
%
%see also: First2Turns.find2turnstrajectory6Err First2Turns.findrespmat2turns

r0 = obj.rmodel; % ring lattice

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addOptional(p,'indBPM',obj.indBPM,@isnumeric);
addOptional(p,'indHCor',obj.indHst,@isnumeric);
addOptional(p,'indVCor',obj.indVst,@isnumeric);
addOptional(p,'kick',1e-4,@isnumeric);
addOptional(p,'delta',1e-3,@isnumeric);
addOptional(p,'RF',true,@islogical);
addOptional(p,'rad',true,@islogical);

parse(p,obj,varargin{:});

%alpha = mcf(r0);
%f0 = obj.getRFFrequency;% atgetfieldvalues(r0,find(atgetcells(r0,'Frequency'),1,'first'),'Frequency');

inCOD             = zeros(6,1);
indBPM            = p.Results.indBPM;
indHorCor         = p.Results.indHCor;
indVerCor         = p.Results.indVCor;
kval              = p.Results.kick;
delta             = p.Results.delta;
rf_on             = p.Results.RF;
radon             = p.Results.rad;

indrfc = findcells(r0,'Frequency');
hn=r0{indrfc(1)}.HarmNumber;
Vt=sum(atgetfieldvalues(r0,indrfc,'Voltage'));

if ~rf_on & ~radon
     warning('no rf and no radiation is not a realistic lattice condition')
   r0 = atsetcavity(r0,0.0,0.0,hn);
elseif ~rf_on & radon  
    r0 = atsetcavity(r0,0.0,1.0,hn);
elseif rf_on & ~radon
    warning('RF on but no radiation is not a realistic lattice condition')
    r0 = atsetcavity(r0,Vt,0.0,hn);
elseif rf_on & radon
    r0 = atsetcavity(r0,Vt,1.0,hn);
end

ModelRM.kval = kval;
ModelRM.delta = delta;

% % for quick test
% indHorCor(4:end)=[];
% indVerCor(4:end)=[];

K = [0,0];
CV = [0,0];
Septa = [0,0];
disp('Simulated Trajectory RM computation in progress ... ')

[t0] = obj.findNturnstrajectory6Err(r0,K,CV,Septa);
%% horizontal
% trajectory
RMTH= obj.findrespmatNturns(indHorCor,kval,'KickAngle',1,1);

RMTH{1}=RMTH{1}./kval;
RMTH{2}=RMTH{2}./kval;
RMTH{3}=RMTH{3}./kval;
RMTH{4}=RMTH{4}./kval;

disp(' --- computed horizontal trajectory RM from model --- ');
% store data
ModelRM.TrajHCor=RMTH;

% temporary store of ModelRM
obj.ModelRM = ModelRM;

%% vertical
RMTV = obj.findrespmatNturns(indVerCor,kval,'KickAngle',1,2);

RMTV{1}=RMTV{1}./kval;
RMTV{2}=RMTV{2}./kval;
RMTV{3}=RMTV{3}./kval;
RMTV{4}=RMTV{4}./kval;

disp(' --- computed vertical trajectory RM from model --- ');

% store data
ModelRM.TrajVCor=RMTV;

% temporary store of ModelRM
obj.ModelRM = ModelRM;

%% dpp
% orbit RM dpp
inCODPdpp=inCOD;
inCODPdpp(5)=delta;% linepass is used.
% o=trajfun(r0,indBPM,inCODPdpp);

o=obj.findNturnstrajectory6Err(r0,K,CV,Septa,inCODPdpp);

oxPdpp=o(1,:);
oyPdpp=o(3,:);
inCODMdpp=inCOD;
inCODMdpp(5)=-delta;

% o=trajfun(r0,indBPM,inCODMdpp);
o=obj.findNturnstrajectory6Err(r0,K,CV,Septa,inCODMdpp);

oxMdpp=o(1,:);
oyMdpp=o(3,:);
dppH=(oxPdpp-oxMdpp)/2/delta;
dppV=(oyPdpp-oyMdpp)/2/delta;
disp(' --- computed trajectory/dpp RM from model --- ');

% store data
ModelRM.TrajHDPP=dppH;
ModelRM.TrajVDPP=dppV;

% temporary store of ModelRM
obj.ModelRM = ModelRM;

%% TL2 septa correctors RM
obj.ModelRM.RMCV=[];
obj.ModelRM.RMSP=[];

cv8rm = obj.findNturnstrajectory6Err(r0,K,CV+[kval 0],Septa)-t0;
cv9rm = obj.findNturnstrajectory6Err(r0,K,CV+[0 kval],Septa)-t0;

rmcv = [ cv8rm(3,:); cv9rm(3,:)];

s12rm = obj.findNturnstrajectory6Err(r0,K,CV,Septa+[kval 0])-t0;
s3rm  = obj.findNturnstrajectory6Err(r0,K,CV,Septa+[0 kval])-t0;

rmsp = -[ s12rm(1,:); s3rm(1,:)]; % septa sign convention inverted

ModelRM.RMCV=rmcv./repmat(kval./[obj.cv8.calibration; obj.cv9.calibration],1,size(indBPM,2)*obj.nturns); % only vertical RM
ModelRM.RMSP=rmsp./repmat(kval./[obj.s12.calibration; obj.s3.calibration] ,1,size(indBPM,2)*obj.nturns); % only horizontal RM

% ModelRM.RMCV=rmcv./repmat(kval./obj.calibcv89,1,size(indBPM,2))*2; % only vertical RM
% ModelRM.RMSP=rmsp./repmat(kval./obj.calibs12 ,1,size(indBPM,2))*2; % only horizontal RM

obj.ModelRM = ModelRM;
disp('A new response matrix has been set for correction');

end
