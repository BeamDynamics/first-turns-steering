classdef FirstTurns < RingControl
    %FIRSTTURNS corrects trajectory to obtain first 2 turns
    %   the class measures N turns trajectory and uses all the correctors
    %   before the last BPM with signal to correct it. 
    %
    %   Specialized class to store methods and properties for
    %   First N turns correction.
    %
    %   Allows :
    %   Measure trajectory RM
    %   correct trajectory for first turns
    %   simulate First Turns trjectory
    %
    %   can be used for: ebs-simu, esrf-sr, esrf-sy
    %
    %   Inherits all methods and properties from: RingControl
    %   
    %   Example of use:
    %   >> ft = FirstTurns('ebs-simu')
    %   >> ft.measuretrajectory
    %   >> plot(ft.last_measured_trajectory')
    %   >> ft.getHSteerersValues
    %   >> ft.correction (corrects trajectory)
    % 
    %   First2Turns.tryme gives a small running example of usage.
    %   
    %see also: RingControl
    
    properties
             

        ModelRM % response matrix structure for trajectory correction
        
        FullTunes
        FullTuneRM
        
        
    end
    
    
    methods
        
        % creator
        function obj = FirstTurns(machine)
            %FIRSTTURNS Construct an instance of this class
            %   
            %   Specialized class to store methods and properties for 
            %   First 2 turns correction.
            %
            %   Allows :
            %   Measure trajectory RM
            %   correct trajectory for first turns
            %   simulate First Turns trjectory
            %
            %   can be used for: ebs-simu, esrf-sr, esrf-sy
            %
            %   Inherits all methods and properties from RingControl:
            %
            %see also: RingControl
            
            obj@RingControl(machine);
            
            obj.savefile = true;
            
            % trajectory correction for 2 turns
            obj.setTurns(10);
            obj.n_acquisitions = 3;
                    
            switch obj.machine
                case 'ebs-simu'
                    
                    b = load('/users/beamdyn/dev/commissioningtools/@RingControl/EBSSimuTrajRM20turns.mat');
                    obj.ModelRM = b.ModelRM;

                case 'ebs-sr'
                    
                    b = load('/users/beamdyn/dev/commissioningtools/@RingControl/EBSSimuTrajRM20turns.mat');
                    obj.ModelRM = b.ModelRM;

                case 'esrf-sr'
                    
                    b = load('/users/beamdyn/dev/commissioningtools/@RingControl/ESRFSRSimulatedRM.mat');
                    obj.ModelRM = b.ModelRM;
                    %c = load('/mntdirect/_machfs/MDT/2018/Nov06/commissioningtools/MeasureESRF-SR_SeptaCVRF_trajectory.mat','ModelRMCVSeptaRF');
                    % obj.ModelRM.RMSP = c.ModelRMCVSeptaRF.RMSP;
                    % obj.ModelRM.RMCV = c.ModelRMCVSeptaRF.RMCV;
                    c = load('/mntdirect/_machfs/MDT/2018/Nov06/commissioningtools/MeasuredSeptaCVRMoffenergy','ModelRM');
                    obj.ModelRM.RMSP = c.ModelRM.RMSP;
                    obj.ModelRM.RMCV = c.ModelRM.RMCV;
                    d = load('/mntdirect/_machfs/MDT/2018/Nov06/commissioningtools/MeasureESRF-SR_RF_trajectory.mat','ModelRMRF');
                    obj.ModelRM.TrajHDPP = d.ModelRMRF.TrajHDPP;
                    
                case 'esrf-sy'
                 
                    b = load('/users/beamdyn/dev/commissioningtools/@RingControl/ESRFSYSimulatedRM.mat');
                    obj.ModelRM = b.ModelRM;
                    
                otherwise
                    error('Suported machines are: ''esrf-sr'', ''esrf-sy'', ''ebs-simu''')
            end
            
        end
        
        % methods defined in seprated functions

        % measure response matrix
        modelRM = MeasureTrajectoryRM(obj,varargin);
       
        % compute response matrix
        modelRM = SimulateTrajectoryRM(obj,varargin);
        % compute RM
        C = findrespmatNturns(obj, PERTURB, PVALUE,FIELD,M,N, varargin)
        % compute 2 turns reference trajectory
        [t0] = findNturnstrajectory6Err(obj,ring,K,CV,S,incod);
        
        % correction function
        [hs,vs,cv,sp,rf] = correction(obj,varargin);
       
        % estimate full tunes
        FullTunes=getFullTunes(obj);
        
        % compute full tune response matrix
        FullTuneRM = SimulateFullTuneRM(obj,varargin);
        
    end
    
    methods (Static)
        
        % test function
        tryme
        
    end
    
end
