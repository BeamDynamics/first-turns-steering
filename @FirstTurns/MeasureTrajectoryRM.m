function ModelRM = MeasureTrajectoryRM(obj,varargin)
% measure trajectory RM (single sided)
% optional inputs:
%          'h',true
%          'v',true
%          'cv',true
%          'sp',true
%          'rf',true
%
% acquire data for 20 turns.
%
%see also: 

simulator = strcmp(obj.machine,'ebs-simu');

initturns = obj.nturns;
obj.setTurns(20);

switch_on_gun_rips = true;

if simulator
    switch_on_gun_rips = false;
end

sfi = obj.savefile;

obj.savefile = false;

waitPStime = 3; % [s]
%
p = inputParser;

addRequired(p,'obj'); % default empty
addOptional(p,'h',true,@islogical); % default empty
addOptional(p,'v',true,@islogical); % default empty
addOptional(p,'cv',true,@islogical); % default empty
addOptional(p,'sp',true,@islogical); % default empty
addOptional(p,'rf',true,@islogical); % default empty

parse(p,obj,varargin{:});

h = p.Results.h;
v = p.Results.v;
cv = p.Results.cv;
sp = p.Results.sp;
measrf = p.Results.rf;

% h=false;
% v=false;
% cv=true;
% sp=true;
% 
% measrf = false;
   
deltarf = 50;
   
% start innjection once and for all

if switch_on_gun_rips
    
    gun  = tango.Device(obj.gun);
    rips = tango.Device(obj.rips);
    
    if ~strcmp(rips.State,'Running')
    rips.StartRamping()
    end
    
    gun.On();
    
    while ~strcmp(rips.State,'Running')
        disp('rips moving!')
        pause(1);
    end
    
end
if ~simulator
    deltak = 1e-5; % rad
else
    deltak = 1e-5; % rad
end
deltaA = 0.01; % Ampere

t0 = obj.measuretrajectory;

enabledcorH = obj.getEnabledHsteerers;
enabledcorV = obj.getEnabledVsteerers;

% % for quick test
% enabledcorH(4:end)=false;
% enabledcorV(4:end)=false;

% H steerers RM
ModelRM.TrajHCor{1} = [];
ModelRM.TrajHCor{3} = [];
ModelRM.TrajVCor{1} = [];
ModelRM.TrajVCor{3} = [];

if h
    hs0 = obj.sh.get;
    for ik = 1:length(hs0) %
        if enabledcorH(ik) % only if eneabled corrector
            disp(['H steerer' num2str(ik)])
            hs = hs0;
            hs(ik) = hs(ik) + deltak;
            
            obj.sh.set(hs);
            
            pause(waitPStime);
            
            t = obj.measuretrajectory;
            
            obj.sh.set(hs0);
            
            rmh(:,ik) = (t(1,:) - t0(1,:))./deltak; % m/rad
            rmv(:,ik) = (t(2,:) - t0(2,:))./deltak;
        else
            disp(['H steerer' num2str(ik) ' is disabled'])
            rmh(:,ik) = NaN(size(t0(1,:)));
            rmv(:,ik) = NaN(size(t0(2,:)));
            
        end
    end
    
    ModelRM.TrajHCor{1} = rmh;
    ModelRM.TrajHCor{3} = rmv;
    
    
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

if measrf
    % RF RM
    disp('RF RM');
    rf0 = obj.rf.get;
    rf = rf0 + deltarf;
    
    obj.rf.set(rf);
    
    pause(waitPStime);
    
    t = obj.measuretrajectory;
    
    obj.rf.set(rf0);
    
    ModelRM.TrajHDPP = (t(1,:) - t0(1,:))./deltarf;
    ModelRM.TrajVDPP = (t(2,:) - t0(2,:))./deltarf;
else
    ModelRM.TrajHDPP = [];
    ModelRM.TrajVDPP = [];
end
obj.ModelRM = ModelRM; %temporary store of ModelRM


% V steerers RM
if v
    vs0 = obj.sv.get;
    for ik = 1:length(vs0) %1:2%
        if enabledcorV(ik) % only if eneabled corrector
            
           disp(['V steerer' num2str(ik) ' is disabled'])
             vs = vs0;
            vs(ik) = vs(ik) + deltak;
            
            obj.sv.set(vs);
            
            pause(waitPStime);
            
            t = obj.measuretrajectory;
            
            obj.sv.set(vs0);
            
            rmh(:,ik) = (t(1,:) - t0(1,:))./deltak;
            rmv(:,ik) = (t(2,:) - t0(2,:))./deltak;
        else
           disp(['V steerer' num2str(ik) ' is disabled'])
             rmh(:,ik) = NaN(size(t0(1,:)));
            rmv(:,ik) = NaN(size(t0(2,:)));
            
        end
    end
    ModelRM.TrajVCor{1} = rmh;
    ModelRM.TrajVCor{3} = rmv;
end
obj.ModelRM = ModelRM; %temporary store of ModelRM


% septa
disp('septa RM')
if sp
    hs0 = [obj.s12.get obj.s3.get];
    for ik = 1:length(hs0)
        
        hs = hs0;
        hs(ik) = hs(ik) + deltak;
        
        obj.s12.set(hs(1));
        obj.s3.set(hs(2));
        
        pause(waitPStime);
        
        t = obj.measuretrajectory;
        
        obj.s12.set(hs0(1));
        obj.s3.set(hs0(2));
        
        ModelRM.RMSP(ik,:) = (t(1,:) - t0(1,:))./deltak;
        
    end
    
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

% CV TL2 steerers RM
disp('CV RM')
if cv
    vs0 = [obj.cv8.get obj.cv9.get];
    for ik = 1:length(vs0)
        
        vs = vs0;
        vs(ik) = vs(ik) + deltaA;
        
        obj.cv8.set(vs(1));
        obj.cv9.set(vs(2));
        
        pause(waitPStime);
        
        t = obj.measuretrajectory;
        
        obj.cv8.set(vs0(1));
        obj.cv9.set(vs0(2));
        
        ModelRM.RMCV(ik,:) = (t(2,:) - t0(2,:))./deltaA;
        
    end
end
obj.ModelRM = ModelRM; %temporary store of ModelRM

if switch_on_gun_rips
    pause(0.25); % rips still moving.
    rips.StopRamping()
    gun.Off(); % limit dose
    %Ke.PulseNumber = pn; % set back initial number of pulses for extraction
end

obj.savefile = sfi;
obj.ModelRM = ModelRM;

obj.setTurns(initturns);

end

