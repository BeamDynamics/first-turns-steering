function [hs,vs,cv,sp,freq]=...
    correction(...
    obj,...
    varargin)
%
% CORRECTION corrects trajectory on first turns
% USING tango Devices on real accelerator (ESRF SR/SY).
%
% This function is a wizard. it will prompt you to continue
% correction, change parameters, compute/measure response matrices,... 
% 
% [ModelRM,hs,vs,cv,sp,freq]=...
%     correction(...
%     obj,...
%     varargin)
%
% OPTIONAL INPUTS:
% 'indBPM'                  bpm indexes
% 'indHCor'                 h. cor indexed
% 'indVCor'                 v. cor indexed
% 'max_BPM_good_reading'    maximum bpm accepted reading (lim+2e-3m for extra search)
%                           (default: [15e-3 (hor.) 3e-3m (ver.)])
% 'bpm_s_range'             BPM in this range are used. [m] (default [0,C]) 
% 'cor_s_range'             COR in this range are used. [m] (default [0,C]) 
% 'accepeted_turns'         accepted number of turns (default 10)
% 'correct_TL2'             correct using TL2 CV89 and Septa
%                           (default =false, added if correction stack)
% 'correct_H'               correct using H steerers (default = true)
% 'correct_V'               correct using V steerers (default = true)
% 'correct_dpp'             correct dpp (default = false)
% 'correctors_mean_zero'    correctors mean constrained to zero
% 'svd_mode'                'Tikhonov' or 'eigenvectors' (defualt)
% 'Tikhonov'                Tikhonov parameter (ignored if mode eignvectors)
% 'ma_num_eig'              maximum number of eigenvectors (ignored if mode Tikhonov)
% 'ModelRM'                 Trajectory response matrix
%                               (if not provided it will be computed and
%                               saved in TrajRM.mat and given as output and
%                               overwrite the obj.ModelRM property).
% 'reference_orbit'         reference trajectory 2xnbpm (default:0)
% 'steerers_max'            2x1 limit of steerers abs(steerer)<steererlimit
%                           (if not given, no limits), (default [3,3]e-4 rad)
% 'InjectedBeamFromSRaxis'  distance of injected beam from SR axis
%                           (default off axis -16mm)
% 'K3K4'                    kickers 3 and 4, elements DR_K3, DR_K4 in
%                           lattice (default -10mm bump for S28F)
% 'ask_to_continue'         ask message to keep iterating (default: true)
% 'verbose'                 print text ( default: true)
%     )
%
% finds closed trajectory:
% correct first 2 turns trajectory with available BPM and correctors
% if not found, increase amplitude limit for bpm reading
%               force use of TL2 CV8 CV9 and septa
%               increase number of eigenvectors used
%
%
% if measured trajectory amplitude is different from imulated one, think
% about changing obj.injoff
%
% EXAMPLE USAGE:
% >> ft=FirstTurns('ebs-simu');
% >> ft.correction
%
% to call only h correction
% >> ft.correction('correct_h',true,'correct_v',false);
%
%see also: FirstTurns.SimulateTrajectoryRM FirstTurns.MeasureTrajectoryRM
%FirstTurns.setTurns FirstTurns.findNturnstrajectory6Err

% variables requird by first two turns RM computation
% injection bump

r0 = obj.rmodel;
C = findspos(r0,length(r0)+1);

tl2=obj.tl2model;

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addOptional(p,'indBPM',obj.indBPM,@isnumeric);
addOptional(p,'indHCor',obj.indHst,@isnumeric);
addOptional(p,'indVCor',obj.indVst,@isnumeric);
addOptional(p,'indSepta',find(atgetcells(tl2,'FamName','TL2_S2','TL2_S3')),@isnumeric);
addOptional(p,'indTL2VCor',find(atgetcells(tl2,'FamName','TL2_VS_08','TL2_VS_09')),@isnumeric);

addOptional(p,'max_BPM_good_reading',[15e-3 3e-3],@isnumeric);
addOptional(p,'cor_s_range',[0 C],@isnumeric);
addOptional(p,'bpm_s_range',[0 C],@isnumeric);
addOptional(p,'accepted_turns',10,@isnumeric);

expectedsvdmodes={'eigenvectors','Tikhonov'};
addParameter(p,'svd_mode','eigenvectors',...
                 @(x) any(validatestring(x,expectedsvdmodes)));
addOptional(p,'max_num_eigen',10,@isnumeric);
addOptional(p,'Tikhonov',10,@isnumeric);
             
addOptional(p,'correct_TL2',false,@islogical);
addOptional(p,'correct_H',true,@islogical);
addOptional(p,'correct_V',true,@islogical);
addOptional(p,'correct_dpp',false,@islogic);
addOptional(p,'correctors_mean_zero',true,@islogical);

addOptional(p,'ModelRM',{},@(x) (iscell(x) || isstruct(x))); % if cell compute, if struct RM already computed
addOptional(p,'reference_orbit','compute',@isnumeric); %
addOptional(p,'steerers_max',[0.0003 0.0003],@isnumeric); % if cell compute, if struct RM already computed
addOptional(p,'verbose',true,@islogical); % if cell compute, if struct RM already computed
addOptional(p,'ask_to_continue',true,@islogical); % if cell compute, if struct RM already computed

parse(p,obj,varargin{:});

alpha = mcf(r0);
f0 = obj.rf.get;% atgetfieldvalues(r0,find(atgetcells(r0,'Frequency'),1,'first'),'Frequency');

inCOD           = zeros(6,1);
indBPM          = p.Results.indBPM;
indHCor         = p.Results.indHCor;
indVCor         = p.Results.indVCor;

lim             = p.Results.max_BPM_good_reading;
cor_s_range     = p.Results.cor_s_range;
bpm_s_range     = p.Results.bpm_s_range;
maxturns        = p.Results.accepted_turns;
correct_TL2     = p.Results.correct_TL2;
correct_H       = p.Results.correct_H;
correct_V       = p.Results.correct_V;
correctflags(1) = p.Results.correct_dpp;
correctflags(2) = p.Results.correctors_mean_zero;

ModelRM         = p.Results.ModelRM;
reforbit        = p.Results.reference_orbit;
steererlimit    = p.Results.steerers_max;
printouttext    = p.Results.verbose;
ask2cont        = p.Results.ask_to_continue;

max_num_eigen   = p.Results.max_num_eigen;
Tikhonov        = p.Results.Tikhonov;
svd_mode        = p.Results.svd_mode;


sbpm1 = findspos(r0,indBPM);
sbpm=[];
for it=1:obj.nturns
    sbpm = [sbpm,C*(it-1)+sbpm1];  % s locations for any number of turns
end
scorh = findspos(r0,indHCor);
scorv = findspos(r0,indVCor);


% trajmeasfun=@(varargin)obj.measuretrajectory(varargin);

obj.savefile = false;
warning('no file saving')

if ischar(reforbit)
    % get 2 turns reference trajectory
    [t0] = obj.findNturnstrajectory6Err(obj.rmodel,[0,0],[0 0],[0 0]);
    reforbit = t0([1,3],:);
else
    t0(1,:) = reforbit(1,:);
    t0(3,:) = reforbit(2,:);
end

traj=0; % initial correction uses model lattice and default parameters.
% if needed will switch to traj=1 during the loop.
if printouttext
    if traj==1
        disp('correcting trajectory');
    elseif traj==0
        disp('correcting trajectory with model rm')
    end
end

%% compute model response matrices or compute them

if iscell(ModelRM) & isempty(obj.ModelRM)
    
    clear ModelRM
    
    if printouttext
        disp('computing model trajectory RM')
    end
    
    obj.SimulateTrajectoryRM;
    
    save(['TrajRM' obj.TrajFileName],'ModelRM');
    
    if printouttext
        disp('computed model trajectory RM')
    end
    
end


% use only obj.nturns matrix
sizehRM = size(obj.ModelRM.TrajHCor{1},1);
first_nan = find(isnan(obj.ModelRM.TrajHCor{1}(1))||isnan(obj.ModelRM.TrajHCor{3}(1)),1,'first');
    if isempty(first_nan)
        first_nan = sizehRM;
    end
max_turns_RM = min( sizehRM./length(indBPM),...
                   floor(first_nan/length(indBPM)));

%% initilize parameters

nbpmuprev=0;
countiter=0; % counuter for iterations.
countstack=0;% chack that correction did not get stack
deltacor = 0; % initializ RF correction value
stopped = false;
firstdata = true;

f = figure;
set(f,'WindowStyle','docked');

countatbpm=zeros(size(reforbit(1,:)));

% correction parameters modifiable by user during correction
corropt.number_of_acquisitions = obj.n_acquisitions;  % n of acquisitions to average
corropt.absolute = false;               % relative (false, default) or absolute (true) correction
corropt.fraction = 0.5;                 % fraction of correction to apply
corropt.limits = lim;                   % h and v limit for acceptable trajectory in [m]
corropt.steererlimit = steererlimit;    % h and v steerer maximum strengths
corropt.maxturns = maxturns;            % number of turns obtained to declare success correction
corropt.max_turns_RM = max_turns_RM;    % user NOT allowed to change this.
corropt.correctturns = obj.nturns;      % turns to correct on
corropt.cor_s_range = cor_s_range;      % range in m where to use correctors
corropt.bpm_s_range = bpm_s_range;      % range in m where to use BPMs
corropt.meanzero = correctflags(2);     % keep horizontal mean to zero
corropt.meanzeroWeigth = 1;             % wiegth for mean zero in correction
corropt.corRF = correctflags(1);        % correct RF
corropt.corH = correct_H;               % correct H
corropt.corV = correct_V;               % correct V
corropt.corTL2 = correct_TL2;           % correct with Septa and CV89
corropt.svdmode = svd_mode;             % method for svd regularization
switch corropt.svdmode
    case expectedsvdmodes{1}
        corropt.svdparam = max_num_eigen;
    case expectedsvdmodes{2}
        corropt.svdparam = Tikhonov;
end
corropt.bpm_s_range = [(sbpm1(2)+sbpm1(3))/2 C]; %start from third BPM

%% get initial trajectory measurement
[t] = obj.measuretrajectory;
ox=t(1,:)-reforbit(1,:);
oy=t(2,:)-reforbit(2,:);
usebpm = selectsignal(ox,oy,corropt);
nbpmu=length(find(usebpm));

% display initial trajectory
obj.plottrajectory('figure',false);

% change correction options
response='a';
disp(corropt);
while ~ismember(response, 'yn')  % & ~ischar(response)
    response = input('Change correction options? [y/n]: ','s');
    if isempty(response), response = 'a'; end
end
if response == 'y'
    corropt = CorrectionParametersUpdate(obj,corropt);
end
            
%% loop until a more than 2 turns are observed
while (nbpmu<=length(indBPM)*corropt.maxturns) && ~stopped
    
    if printouttext
        disp('Search closed orbit');
    end
    
    countiter=countiter+1;
    
    %% get current orbit
    t_iter_start =t;
    if firstdata
        t00 = t;
        firstdata=false;
    end
    
    %% manage loop getting stack :
    % no improvement
    if nbpmuprev==nbpmu % no more bpm then previous correction
        response='a';
        while ~ismember(response, 'yn')  % & ~ischar(response)
            response = input(['no improvement from last step.'...
                ' Change some correction options? [y/n]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'y' % Measure
            
            corropt = CorrectionParametersUpdate(obj,corropt);
        
        end
        
    else
        countstack=0;
    end
    
    % too many iterations without progress
    if countstack==10
        error('Could not find closed orbit. Aborting trajectory correction');
    end
    
    % no beam
    if nbpmu==0
        error('NO BEAM AT ALL!')
    end
    
    % get RM (if obj.nturns changed, larger RM, longer trajectory)
    if corropt.correctturns~=obj.nturns
        
        warning('# of turns has changed.');
        obj.setTurns(corropt.correctturns);
        
        disp('update reference orbit');
        [t0] = obj.findNturnstrajectory6Err(obj.rmodel,[0,0],[0 0],[0 0]);
        reforbit = t0([1,3],:);
        
        disp('update counters');
        countatbpm=zeros(size(reforbit(1,:)));

        disp('update BPM s positions');
        sbpm=[];
        for it=1:obj.nturns
            sbpm = [sbpm,C*(it-1)+sbpm1];  % s locations for any number of turns
        end
        
        disp('measure new trajectory')
        [t] = obj.measuretrajectory;
        
        % remove reference
        ox=t(1,:)-reforbit(1,:);
        oy=t(2,:)-reforbit(2,:);
        
        usebpm = selectsignal(ox,oy,corropt);
        nbpmu = length(find(usebpm));
        lastsig = find(~isnan(ox),1,'last');
        
        t_iter_start = t;
        
    end
    
    % select traejctory response matrix for requested number of turns
    disp('select RM elements');
    [RMH,RMV,RMHd,RMVd,RMCV,RMSP] = NturnsRM(obj,max_turns_RM,indBPM);
    
    %% select all available correctors before last bpm reading signal
    if obj.nturns>1
        indBPMp1=[indBPM length(r0)+indBPM(1)]; % add 1 bpm from second turn to include last corrector
        
        usecorH=indHCor<max( indBPMp1( usebpm( 1:length(indBPMp1) ) ) );
        usecorV=indVCor<max( indBPMp1( usebpm( 1:length(indBPMp1) ) ) );
    else % 1 turn correction case
        indBPMp1=[indBPM ]; % add 1 bpm from second turn to include last corrector
        
        usecorH=indHCor<max( indBPMp1( usebpm( 1:length(indBPMp1) ) ) );
        usecorV=indVCor<max( indBPMp1( usebpm( 1:length(indBPMp1) ) ) );
    end
    
    nhcu=length(find(usecorH));
    nvcu=length(find(usecorV));
    
    countatbpm(nbpmu)=countatbpm(nbpmu)+1;
    
    if nhcu==0 || nvcu==0
        error('NO BEAM AT ALL!')
    end
    
    if printouttext
        disp(['Trajectory correction:'...
            ' turns= ' num2str( floor( nbpmu/length(indBPM) ) )...
            ' nbpms= ' num2str( mod( nbpmu,length(indBPM) ) )...
            ' ncor: ' num2str([nhcu nvcu],'%d, ')]);
    end
    
    % limit to selection of correctors
    rangehcorsel = scorh  > corropt.cor_s_range(1) & scorh  < corropt.cor_s_range(2);
    rangevcorsel = scorv  > corropt.cor_s_range(1) & scorv  < corropt.cor_s_range(2);
    rangebpmsel1 = sbpm1 > corropt.bpm_s_range(1) & sbpm1 < corropt.bpm_s_range(2);
    rangebpmsel = repmat(rangebpmsel1,1,obj.nturns);
    
    % get lists of disabled BPM and correctors
    % (here, so that BPM and correctors can be disabled on the fly, during correction)
    enabledbpm  = repmat(obj.getEnabledBPM,1,obj.nturns);
    enabledcorH = obj.getEnabledHsteerers;
    enabledcorV = obj.getEnabledVsteerers;
    
    %         signal ok     & user selected s range     & enabled
    usecorH = usecorH       & rangehcorsel               & enabledcorH;
    usecorV = usecorV       & rangevcorsel               & enabledcorV;
    usebpm  = usebpm        & rangebpmsel               & enabledbpm;
    
    if length(find(usebpm))==1
        disp('only 1 BPM with good signal. Including first 2 bpms (whatever their signal)');
        usebpm([1,2])=true;
    end
    %% ----- CORRECTION ----- %%
    
    % signal to correct
    X=ox(usebpm)';
    Y=oy(usebpm)';
    
    
    %% COMPUTE HORIZONTAL CORRECTION
    % septa, h steerers, frequency
    
    if printouttext
        disp('H PLANE');
    end
    
    % get initial corrector values
    corh0=obj.sh.get;
    
    % restrict RM to usable correctors
    
    if corropt.corH
        RespH=RMH(usebpm,usecorH);
        steermeanlist = ones(size(corh0(usecorH)));
    else
        RespH=[];
        steermeanlist=[];
    end
    
    % dpp correction, add after of correctors
    if corropt.corRF 
        RespHd=RMHd(usebpm);
    end
    
    % dpp correction, add haed of correctors
    if corropt.corTL2
        if printouttext
            disp('Septa correction');
        end
        
        RespH  = [RMSP(:,usebpm)',RespH];
        steermeanlist = [zeros(1,size(RMSP,1)), steermeanlist];
        
        Septas = [obj.s12.get obj.s3.get];
        
        S120 = Septas(1);
        S30 = Septas(2);
    end
    
    % weigth for correctors average =0
    steermeanlist = steermeanlist*corropt.meanzeroWeigth;
    
    % build RMs
    if      corropt.corRF &&  corropt.meanzero % dpp and mean0
        FRH=[ [RespH; steermeanlist] [RespHd';0] ];
    elseif  corropt.corRF && ~corropt.meanzero % dpp no mean 0
        FRH=[ RespH RespHd' ];
    elseif ~corropt.corRF &&  corropt.meanzero % no dpp mean0
        FRH=[RespH; steermeanlist];
    elseif ~corropt.corRF && ~corropt.meanzero % no dpp no mean0
        FRH=RespH;
    end
    
    % if there are NaN in the RM, prompt for RM Measurement or Simulation
    if any(isnan(FRH))
        
        response='a';
        while ~ismember(response, 'ms')  % & ~ischar(response)
            response = input(['Horizontal Trajectory RM contains NaNs. '....
                'The correction will be stopped.'....
                'Measure or Simulate RM? [m/s]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureTrajectoryRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateTrajectoryRM;
            
        end
        
        % end function here.
        break
    end
    
    % if mean0 correction add 0 to correction vector
    if corropt.meanzero 
         vec = [X;0];
    else
         vec = X;
    end
    
    % get present list of correctors
    if corropt.corTL2
        c0 = [S120 S30 corh0(usecorH)]';
    else
        c0 = corh0(usecorH)';
    end
    
    % if absolute correction, add present correctors x RM
    if corropt.absolute
        
        % get reducen number of eigenvectors RM
%         [U,S,V]=svd(FRH);
%         S(corropt.svdparam+1:end,corropt.svdparam+1:end)=0;
%         MM = U*S*V'; 
        MM= FRH;
        vec0 = MM*c0;
        vec = vec + vec0;
        
        if corropt.meanzero
            vec(end)=0;
        end
    end
    
    
    % SVD/bestcor Correction
    if ~isempty(FRH)
        dch=obj.computecorrection(corropt.svdmode,FRH,vec,corropt.svdparam)*corropt.fraction;
    else
        dch = zeros(size(c0));
    end
    if corropt.absolute
        dch = dch - c0; 
    end
    
    % separate septa and steerers+RF
    if corropt.corTL2 == true
        S12=-dch(1)+S120; 
        S3=-dch(2)+S30;
        
        dch = dch(3:end);
    end
    
    % if dpp correction separate dpp from correctors
    if corropt.corRF
        %    inCOD(5)=inCOD(5)+dch(end);
        deltacor0=deltacor;
        deltacor=dch(end);
        dch=dch(1:end-1);
    end
    
    tothcor=corh0;
    
    % limit steerers strengths
    if corropt.corH
        cc = corh0*0;
        cc(usecorH) = dch;
        tothcor=corh0-cc;
        
    end
    
    
    %% COMPUTE VERTICAL CORRECTION
    if printouttext
        disp('V PLANE');
    end
    
    % get current correctors values
    corv0=obj.sv.get;
    
    % restrict RM
    RespV=RMV(usebpm,usecorV);
    steermeanlist = ones(size(corv0(usecorV)));
    
    if corropt.corRF % dpp correction
        RespVd=RMVd(usebpm);
    end
    
    if ~corropt.corV
        RespV=[];
        steermeanlist=[];
   end
    

    if corropt.corTL2
        if printouttext
            disp('CV8-9 correction');
        end
        RespV  = [RMCV(:,usebpm)',RespV];
        steermeanlist = [zeros(1,size(RMCV,1)), steermeanlist];
        
        TL2CV = [obj.cv8.get obj.cv9.get];
        
        CV80 = TL2CV(1);
        CV90 = TL2CV(2);
        
    end
    
    % weigth for correctors average =0
    steermeanlist = steermeanlist*corropt.meanzeroWeigth;
    
    % build RMs
    if  corropt.meanzero % no dpp mean0
        FRV=[RespV;steermeanlist];
    elseif ~corropt.meanzero % no dpp no mean0
        FRV=RespV;
    end
    
    
    % if there are NaN in the RM, prompt for RM Measurement or Simulation
    if any(isnan(FRV))
        
        response='a';
        while ~ismember(response, 'ms')  % & ~ischar(response)
            response = input(['Vertical Trajectory RM contains NaNs. '....
                'The correction will be stopped.'....
                'Measure or Simulate RM? [m/s]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureTrajectoryRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateTrajectoryRM;
            
        end
        
        % end function here.
        break
    end
    
    % if mean0 correction add 0 to correction vector
    if corropt.meanzero 
         vec = [Y;0];
    else
         vec = Y;
    end
    
    % get present list of correctors
    if corropt.corTL2
        c0 = [CV80 CV90 corv0(usecorV)]';
    else
        c0 = corv0(usecorV)';
    end
    
    % if absolute correction, add present correctors x RM
    if corropt.absolute
        vec = vec + FRV*c0 ; 
    end
    
    % SVD/bestcor Correction
    if ~isempty(FRV)
        dcv=obj.computecorrection(corropt.svdmode,FRV,vec,corropt.svdparam)*corropt.fraction;
    else
        dcv = zeros(size(c0));
    end
    
    if corropt.absolute
        dcv = dcv - c0; 
    end
    
    %     % if mean0 correction add 0 to correction vector
%     if corropt.meanzero
%         dcv=obj.svd(corropt.svdmode,FRV,[Y;0],corropt.svdparam)*corropt.fraction;
%     else
%         dcv=obj.svd(corropt.svdmode,FRV,Y,corropt.svdparam)*corropt.fraction;
%     end
    
    % separate CV and steerers
    if corropt.corTL2
        
        CV8=-dcv(1)+CV80;
        CV9=-dcv(2)+CV90;
        dcv = dcv(3:end);
        
    end
    
    totvcor=corv0;
    
    if corropt.corV
        % limit steerers strengths
        cc = corv0*0;
        cc(usecorV) = dcv;
        totvcor=corv0-cc;
      
    end
    
    %% APPLY CORRECTION
    
    if corropt.corTL2
        
        obj.s12.set(S12)
        obj.s3.set(S3)
        
    end
    
    if corropt.corH
        
        obj.sh.set(tothcor);
    end
    
    if corropt.corRF
        
        obj.rf.set(f0-alpha*(deltacor)*f0);
        
    end
    
    if corropt.corTL2
        
        obj.cv8.set(CV8)
        obj.cv9.set(CV9);
        
    end
    
    if corropt.corV
        
        obj.sv.set(totvcor);
        
    end
    
    if printouttext
        disp('correcting trajectory'); end
    
    %% MEASURE AFTER CORRECTION and display small summary
    % get current trajectory
    
    pause(3); % wait steerers PS and for new data from BPMs TbT (MuST BE LARGER THAN LOOP TIME IN SIMULATOR!)
    t0is = t_iter_start;
    disp('measure after correction');
    
    % measure and use trajectory RM
    [t] = obj.measuretrajectory;
    
    % remove reference
    oxc=t(1,:)-reforbit(1,:);
    oyc=t(2,:)-reforbit(2,:);
    
    if printouttext
        % display results
        disp(['hor. : ' num2str(std2(ox(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(oxc(usebpm))*1e6,'%3.3f') ' um'])
        disp(['ver. : ' num2str(std2(oy(usebpm))*1e6,'%3.3f') ' -> '...
            num2str(std2(oyc(usebpm))*1e6,'%3.3f') ' um'])
        disp(['H cor mean: ' num2str(mean(tothcor*1e6),'%3.3f') ' +/- (std)' num2str(std2(tothcor*1e6),'%3.3f') ' murad']);
        disp(['V cor mean: ' num2str(mean(totvcor*1e6),'%3.3f') ' +/- (std)' num2str(std2(totvcor*1e6),'%3.3f') ' murad']);
        
        if corropt.corRF
            disp(['RF : ' num2str(f0) ' --> ' num2str(alpha*(deltacor)*f0,'%3.3f') ' Hz'])
        end
        
        if corropt.corTL2
            disp(['S12: ' num2str(S120) '-->' num2str(S12)]);
            disp(['S3: ' num2str(S30) '-->' num2str(S3)]);
            disp(['CV8: ' num2str(CV80) '-->' num2str(CV8)]);
            disp(['CV9: ' num2str(CV90) '-->' num2str(CV9)]);
        end
        
        % estimate tunes
        [tunesfftx] = findtunetom2(obj.last_measured_trajectory(1,~isnan(obj.last_measured_trajectory(1,:)))',200,1);
        [tunesffty] = findtunetom2(obj.last_measured_trajectory(2,~isnan(obj.last_measured_trajectory(2,:)))',200,1);
        tunesfft =[tunesfftx tunesffty];
        lastbpm=find(~isnan(t0is(1,:)),1,'last');
        [linopt,tuneexpected,~]=atlinopt(obj.rmodel,0,1:obj.indBPM(min(length(obj.indBPM),lastbpm)));
        phlastbpm=linopt(end).mu./2./pi;
        
        
        clf(f);
        set(0,'currentfigure',f);
        ax1=subplot(2,2,1);
        plot(sbpm,reforbit(1,:),'k:'); hold on;
        plot(sbpm,t0is(1,:),'r+-'); hold on;
        plot(sbpm(~usebpm),t0is(1,~usebpm),'mx','MarkerSize',15,'LineWidth',2);
        plot(sbpm,t(1,:),'bo-');
        legend('objective','measured','not used','corrected');
        ylabel('hor [m]'); 
        title({['tunes expected: ' num2str(tuneexpected) ', tunes fft: ' num2str(tunesfft)];...
    ['expected \mu@BPM' num2str(min(length(obj.indBPM),lastbpm)) ': ' num2str(phlastbpm)]});
        %grid on;
        ylim([-corropt.limits(1) corropt.limits(1)]);
        %xlabel('s [m]');
        ax1.XTick=sbpm;
        if strcmp(obj.machine,'ebs-simu')
            ax1.XTickLabel=repmat(cellfun(@(a)a(22:end),ebs.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
        elseif strcmp(obj.machine,'esrf-sr')
            ax1.XTickLabel=repmat(cellfun(@(a)a(12:end),sr.bpmname(1:length(indBPM)),'un',0),1,obj.nturns);
        end
        ax1.XTickLabelRotation=90;
        ax1.FontSize=8;
        ax1.TickLength=[0.0 0.0];
        for it = 1:obj.nturns
            %line([C,C]*it,[-corropt.limits(1),corropt.limits(1)]);
            if it ==1
                text(C*it,corropt.limits(1),['! ' num2str(it) 'turn'],'FontSize',8,'Color',[1 0 0]);
            else
                text(C*it,corropt.limits(1),['! ' num2str(it) 'turns'],'FontSize',8,'Color',[1 0 0]);
            end
        end 
        
        ax2=subplot(2,2,2);
        bar(scorh,repmat(obj.sh.get,1,1));
        ylabel('hor [rad]');
        %xlabel('s [m]');
        ax2.XTick=scorh;
        if strcmp(obj.machine,'ebs-simu')
            ax2.XTickLabel=cellfun(@(a)a(1:end),ebs.hsteername(1:length(indHCor)),'un',0);
        elseif strcmp(obj.machine,'esrf-sr')
            ax2.XTickLabel=repmat(cellfun(@(a)a(1:end),sr.steername(1:length(indHCor)),'un',0),1,obj.nturns);
        end
        ax2.XTickLabelRotation=90;
        ax2.FontSize=8;
        ax2.TickLength=[0.0 0.0];
        ax3=subplot(2,2,3);
        plot(sbpm,reforbit(2,:),'k:'); hold on;
        plot(sbpm,t0is(2,:),'r+-'); hold on;
        plot(sbpm(~usebpm),t0is(2,~usebpm),'mx','MarkerSize',15,'LineWidth',2);
        plot(sbpm,t(2,:),'bo-');
        legend('objective','measured','not used','corrected');
        ylabel('ver [m]'); grid on;
        ylim([-corropt.limits(2) corropt.limits(2)]);
        xlabel('s [m]');
        for it = 1:obj.nturns
            %line([C,C]*it,[-corropt.limits(1),corropt.limits(1)]);
            if it ==1
                text(C*it,corropt.limits(2),['! ' num2str(it) 'turn'],'FontSize',8,'Color',[1 0 0]);
            else
                text(C*it,corropt.limits(2),['! ' num2str(it) 'turns'],'FontSize',8,'Color',[1 0 0]);
            end
        end
        
        ax4=subplot(2,2,4);
        bar(scorv,repmat(obj.sv.get,1,1));
        ylabel('ver [rad]');
        xlabel('s [m]');
        linkaxes([ax1 ax3],'x');
        linkaxes([ax2 ax4],'x');
        
        
    end
    
    %% check final result and promp for next steps
    
    ox = oxc;
    oy = oyc;
    
    if countiter>1
        nbpmuprev=nbpmu;% store previous value
        lastsigprev = lastsig;
    end
    
    usebpm = selectsignal(ox,oy,corropt);
    nbpmu = length(find(usebpm));
    lastsig = find(~isnan(ox),1,'last');
    
    
    if nbpmu>=length(indBPM)*corropt.maxturns | lastsig == length(indBPM)*corropt.maxturns
        msg = ['First ' num2str(corropt.maxturns) ' turns found'];
        disp(repmat('*',1,length(msg)));
        disp(msg);
        disp(repmat('*',1,length(msg)));
        stopped = true;
    end
    
    if nbpmu<nbpmuprev
        
        disp('Correction regressing ');
        disp(['Less BPM good: ' num2str(nbpmu)]);
        disp(['Before Correction: ' num2str(nbpmuprev)]);
        disp(['Less BPM see signal: ' num2str(lastsig)]);
        disp(['Before Correction: ' num2str(lastsigprev)]);
        
        response='a';
        while ~ismember(response, 'kr')  % & ~ischar(response)
            response = input('Keep or Revert? [k/r]: ','s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'r' % REVERT
            
            if corropt.corV
                obj.sv.set(corv0);
            end
            if corropt.corH
                obj.sh.set(corh0);
            end
            if corropt.corTL2
                obj.cv8.set(CV80);
                obj.cv9.set(CV90);
                obj.s12.set(S120);
                obj.s3.set(S30);
            end
            
            if corropt.corRF
                obj.rf.set(f0-alpha*(deltacor0)*f0);
            end
            disp('Change correction parameters to improve next correction')
            corropt = CorrectionParametersUpdate(obj,corropt);
        end
        
        
    end
    
    
    if ask2cont && ~stopped
        response='a';
        while ~ismember(response, 'yn')  % & ~ischar(response)
            response = input('Continue trajectory correction? [y/n]: ','s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'y'
            
            stopped = false;
            
            % chang correction options
            response='a';
            while ~ismember(response, 'yn')  % & ~ischar(response)
                response = input('Change correction options? [y/n]: ','s');
                if isempty(response), response = 'a'; end
            end
            if response == 'y'
                corropt = CorrectionParametersUpdate(obj,corropt);
            end
            
            
        else
            stopped = true;
            disp('correction stopped')
        end
    end
    
    
end % loop until 2 turns

%% display final result
if printouttext
    
    ox=t00(1,:);
    oy=t00(2,:);
    
    t = obj.last_measured_trajectory;
    
    oxc=t(1,:);
    oyc=t(2,:);
    
    
    % display results
    disp('absolute orbit measured (no reference subtracted)')
    disp(['Hor. : ' num2str(std2(ox)*1e6,'%3.3f') ' -> '...
        num2str(std2(oxc)*1e6,'%3.3f') ' um'])
    disp(['Ver. : ' num2str(std2(oy)*1e6,'%3.3f') ' -> '...
        num2str(std2(oyc)*1e6,'%3.3f') ' um'])
end

% % save final trajectory and correctors
% fn=obj.TrajFileName ;
% obj.TrajFileName = [fn 'Final'];
% obj.measuretrajectory;
% obj.TrajFileName = fn ;

% read final steerer settings
if corropt.corH
    hs = obj.sh.get;
else
    disp('wished h steerers values, not set')
    hs = corh0-dch;
end

if corropt.corV
    vs = obj.sv.get;
else
    disp('wished h steerers values, not set')
    vs = corv0-dcv;
end

if corropt.corTL2
    sp = [obj.s12.get obj.s3.get];
    cv = [obj.cv8.get obj.cv9.get];
else
    disp('wished septa and CV89 values, not set')
    sp = [NaN NaN];
    cv = [NaN NaN];
end


if corropt.corRF
    freq = obj.rf.get;
else
    disp('wished frequency change')
    freq = f0-alpha*(deltacor)*f0;
end

end

function corropt = CorrectionParametersUpdate(obj,corropt)
% CorrectionParametersUpdate update correction parameters at user request
%
% corropt.tikhonovParam = 5;
% corropt.cor_selection = true(size(indHCor));
% corropt.bpm_selection = true(size(indBPM));
% corropt.fraction = 0.5;
% corropt.limits = lim;
% corropt.steererlimit = steererlimit;

disp('current correction options')
disp(corropt)

corropt.limits(1)       = inputcor('Maximum horizontal excursion',corropt.limits(1));
corropt.limits(2)       = inputcor('Maximum vertical excursion' ,corropt.limits(2));
corropt.fraction        = inputcor('Fraction of computed correction to be set', corropt.fraction);
corropt.number_of_acquisitions        = inputcor('# of trajectories to average', corropt.number_of_acquisitions);
obj.n_acquisitions = corropt.number_of_acquisitions;
corropt.steererlimit(1) = inputcor('Maximum hor. steerer strength', corropt.steererlimit(1));
corropt.steererlimit(2) = inputcor('Maximum ver. steerer strength', corropt.steererlimit(2));
obj.sh.min= - corropt.steererlimit(1);
obj.sh.max= + corropt.steererlimit(1);
obj.sv.min= - corropt.steererlimit(2);
obj.sv.max= + corropt.steererlimit(2);

corropt.maxturns        = inputcor('accepted number of turns to end correction',corropt.maxturns);
corropt.correctturns    = inputcor(['number of turns to correct (change RM size, max ' num2str(corropt.max_turns_RM) ')'],corropt.correctturns);
if corropt.correctturns > corropt.max_turns_RM
   disp('too many turns for availalbe RM');
   
   response='a';
    while ~ismember(response, 'smk')  % & ~ischar(response)
        response = input(['RM size not correct. Simulate,'...
            ' Measure or Keep maximum? [s/m/k]: '],'s');
        if isempty(response), response = 'a'; end
    end
    
    if response == 'm' % Measure
        prevnturn = obj.nturns;
        obj.setTurns(corropt.correctturns)
        obj.MeasureTrajectoryRM;
        obj.setTurns(prevnturn)
        
    elseif response == 's' % Simulate
        prevnturn = obj.nturns;
        obj.setTurns(corropt.correctturns)
        obj.SimulateTrajectoryRM;
        obj.setTurns(prevnturn)
        
    elseif response == 'k' % Simulate
        
        corropt.correctturns = corropt.max_turns_RM;
        
    end
   
end

if corropt.correctturns <= 0
    disp('please inject positrons.')
    corropt.correctturns = 1;
    pause(1);
    disp('Have fun, turns set to one.')
end

corropt.cor_s_range     = inputcor('use correctors in this range [m]',corropt.cor_s_range);
corropt.bpm_s_range     = inputcor('use BPMs in this range [m]',corropt.bpm_s_range);
corropt.svdmode         = inputcor('svd mode [''eigenvectors/Tikhonov/BestCor''], type [e/t/b] ',corropt.svdmode,false,false,true);
switch corropt.svdmode
    case 't'
        corropt.svdparam       = inputcor('Tikhonov parameter ',corropt.svdparam);        
    case 'e'
        corropt.svdparam       = inputcor('max num of eigenvectors ',corropt.svdparam);        
    case 'b'
        disp('no parameter for best cor');        
    otherwise
        disp('mode not ''e'' or ''t''. default to ''e''');
        corropt.svdmode = 'e';
        corropt.svdparam       = inputcor('max num of eigenvectors ',corropt.svdparam);            
end
corropt.corH            = inputcor('apply horizontal correction',corropt.corH);
corropt.corV            = inputcor('apply vertical correction',corropt.corV);
corropt.corRF           = inputcor('apply RF correction',corropt.corRF);
% corropt.absolute        = inputcor('Absolute (true) or Relative (false) correction',corropt.absolute);
%corropt.meanzero        = inputcor('keep correctors mean to zero',corropt.meanzero);
corropt.meanzeroWeigth  = inputcor('keep correctors mean to zero weigth',corropt.meanzeroWeigth);
if corropt.meanzeroWeigth == 0
    corropt.meanzero = false;
else
    corropt.meanzero = true;
end

corropt.corTL2          = inputcor('apply TL2 correction',corropt.corTL2);

    function n = inputcor(msg,val,showdef,validatesize,textinput)
        if nargin<3, showdef = true; end
        if nargin<4, validatesize = true; end
        if nargin<5, textinput = false; end % 's'
        
        if showdef
            msg = [msg ' [' num2str(val) ']: '];
        else
            msg = [msg ': '];
        end
        
        if ~textinput
            n = input(msg);
        else
            n = input(msg,'s');
        end
        
        if isempty(n)
            n=val;
        end
        
        if validatesize
            if any(size(n)~=size(val))
                disp(['Size of input should be ' num2str(size(val)) ', found ' num2str(size(n))])
                n = inputcor(msg,val,showdef);
            end
        end
        
    end


end


function usebpm = selectsignal(ox,oy,corropt)
% select BPM readings to use for correction

usebpm=~isnan(ox) & ~isnan(oy) &....
    ox <  corropt.limits(1) &....
    ox > -corropt.limits(1) &...
    oy <  corropt.limits(2) &...
    oy > -corropt.limits(2);

% remove spikes from BPM readings
stdx=std(ox(usebpm));
stdy=std(oy(usebpm));

usebpm=~isnan(ox) & ~isnan(oy) &...
    ox < corropt.limits(1) & ox > -corropt.limits(1) &...
    oy < corropt.limits(2) & oy > -corropt.limits(2) &...
    abs(ox) < 3*stdx & abs(oy) < 3*stdy;

end


function [RMH,RMV,RMHd,RMVd,RMCV,RMSP] = ...
    NturnsRM(...
    obj,...
    max_turns_RM,...
    indBPM)
% reduce ModelRM to n turns only

selbpm = true(1,length(indBPM)*obj.nturns);

if length(selbpm)<=size(obj.ModelRM.TrajHCor{1},1)
    
    RMH  = obj.ModelRM.TrajHCor{1}(selbpm,:);
    RMV  = obj.ModelRM.TrajVCor{3}(selbpm,:);
    RMHd = obj.ModelRM.TrajHDPP(:,selbpm);
    RMVd = obj.ModelRM.TrajVDPP(:,selbpm);
    
    RMCV = obj.ModelRM.RMCV(:,selbpm);
    RMSP = obj.ModelRM.RMSP(:,selbpm);
    
else
    warning(['RM too small.'...
        'Requested ' num2str(obj.nturns) ' turns. '...
        'Available: ' num2str(max_turns_RM) ' turns. '])
end

%%  check matrix size is correct
if exist('RMH','var')
    
    disp('Check MATRIX size:');
    disp(['traj. # turns: ' num2str(obj.nturns)]);
    disp(['tot.  # bpm: ' num2str(length(obj.indBPM))]);
    disp(['tot.  # hst: ' num2str(length(obj.indHst))]);
    disp(['tot.  # vst: ' num2str(length(obj.indVst))]);
    exprmsize=[length(indBPM)*obj.nturns length(obj.indHst)];
    disp(['expected hor. RM size: ' num2str(exprmsize)]);
    disp([' ModelRM hor. RM size: ' num2str(size(RMH))]);
    if any(size(RMH) ~= exprmsize)
        
        response='a';
        while ~ismember(response, 'sme')  % & ~ischar(response)
            response = input(['RM size not correct. Simulate,'...
                ' Measure or Exit? [s/m/e]: '],'s');
            if isempty(response), response = 'a'; end
        end
        
        if response == 'm' % Measure
            
            obj.MeasureTrajectoryRM;
            
        elseif response == 's' % Simulate
            
            obj.SimulateTrajectoryRM;
            
        elseif response == 'e' % Simulate
            
            return
        end
        
    end
end

end



