function FullTunes = getFullTunes(obj,varargin)
%
%
% optional name-value inptus:
% 'number_of_units', (1) : W.P. + -nu:1:nu  in hor. and ver. are tested
% 'kickh', 1e-4 : kick for trajectory difference in H
% 'kickv', 1e-4 : kick for trajectory difference in V
% 
% changes tune by +/- nu units compared to nominal and stores simulated
% trajectory response. 
% 
%see also: @RingControl.measuretrajectory @TuneEstimate.SimulateFullTuneRM
  
r0 = obj.rmodel; % ring lattice

%% parse inputs
p = inputParser();
addRequired(p,'obj');
addOptional(p,'number_of_units',1,@isnumeric);
addOptional(p,'kickh',1e-4,@isnumeric);
addOptional(p,'kickv',1e-4,@isnumeric);

parse(p,obj,varargin{:});

inCOD            = zeros(6,1);
nu               = p.Results.number_of_units;
kickh            = p.Results.kickh;
kickv            = p.Results.kickv;

% compute simulated full turn RM
if isempty(obj.FullTuneRM)
    obj.SimulateFullTuneRM; % compute FullTuneRM
end

% measure trajectory reference
disp('reference trajectory measurement');
t0 = obj.measuretrajectory;

% power first h steerer
disp('h kick trajectory measurement');
h0 = obj.sh.get;
dh = zeros(size(h0));
dh(1)=kickh;
obj.sh.set(h0+dh);

th = obj.measuretrajectory;
obj.sh.set(h0);

% poweer first v steerer
disp('v kick trajectory measurement');
v0 = obj.sv.get;
dv = zeros(size(v0));
dv(1)=kickv;
obj.sv.set(v0+dv);

tv = obj.measuretrajectory;
obj.sv.set(v0);

disp('closest tune estimate');
dth = (th(1,:)-t0(1,:))./kickh;
dtv = (tv(2,:)-t0(2,:))./kickv;

selh = ~isnan(dth);
selv = ~isnan(dtv);

valh = obj.FullTuneRM.TH(:,selh)/dth(selh);
[~,ih]=min(valh);
Qh_estimate = obj.FullTuneRM.QH(ih,1); 
figure;
subplot(4,1,1:3);
plot(dth,'LineWidth',3);
hold on;
for iu=1:size(obj.FullTuneRM.TH,1)
    plot((obj.FullTuneRM.TH(iu,selh)*valh(iu))','DisplayName',['Qh = ' num2str(obj.FullTuneRM.QH(iu,1))]);
end
legend
xlabel('BPM #')
ylabel(' h trajectory/kick [m/rad]')
grid on;

subplot(4,1,4);
hold on;
for iu=1:size(obj.FullTuneRM.TH,1)
    plot((obj.FullTuneRM.TH(iu,selh)*valh(iu))'-dth(selh)','DisplayName',['Qh = ' num2str(obj.FullTuneRM.QH(iu,1))]);
end
legend
xlabel('BPM #')
ylabel(' h trajectory/kick -measured [m/rad]')
grid on;



valv = obj.FullTuneRM.TV(:,selv)/dtv(selv);
[~,iv]=min(valh);
Qv_estimate = obj.FullTuneRM.QV(iv,2); 
figure;
subplot(4,1,1:3);
plot(dtv,'LineWidth',3);
hold on;
for iu=1:size(obj.FullTuneRM.TV,1)
    plot((obj.FullTuneRM.TV(iu,selv)*valv(iu))','DisplayName',['Qv = ' num2str(obj.FullTuneRM.QV(iu,2))]);
end
legend
xlabel('BPM #')
ylabel(' v trajectory/kick [m/rad]')
grid on;

subplot(4,1,4);
hold on;
for iu=1:size(obj.FullTuneRM.TV,1)
    plot((obj.FullTuneRM.TV(iu,selv)*valv(iu))'-dtv(selv)','DisplayName',['Qv = ' num2str(obj.FullTuneRM.QV(iu,2))]);
end
legend
xlabel('BPM #')
ylabel(' h trajectory/kick -measured [m/rad]')
grid on;


FullTunes = floor([Qh_estimate Qv_estimate]);
obj.FullTunes = FullTunes;

[tunesfftx] = findtunetom2(dth(selh)',10,1);
[tunesffty] = findtunetom2(dtv(selv)',10,1);
tunesfft =[tunesfftx tunesffty];

lastbpm=find(~isnan(t0(1,:)),1,'last');
[l,t,~]=atlinopt(obj.rmodel,0,1:obj.indBPM(min(length(obj.indBPM),lastbpm))+1);
phlastbpm=l(end).mu./2./pi;

disp(tunesfft)
disp(FullTunes)
disp(phlastbpm)

end
